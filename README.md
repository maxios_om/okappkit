
##OKAppKit 
collection of classes and utilities 
 
## Contents
 - Abstraction 
     + AbstractModel protocol: Interface as contract which provide basic set of methods confirming class should provide to support serialising and de-serialising models from and to JSON dictionary.
     + AbstractModelDB protocol: Interface as contract which provides basic set of methods confirming class should provide to support data base save/update feature.
     + AbstractModel: Base Class which serves placeholder implementations what child class receives by default and can override for customisations.

- NonUI helpers
    + NSDate helpers
    + NSobject helpers
    + Localization helpers
    + Reachability support.
    + DB: FMDB & version migrator for FMDB
    + Macros: 
    + NSArray helpers
    + NSDictionary helpers
    + NSString helpers

- UI Helpers
    + [ALAlertBanner](https://github.com/lobianco/ALAlertBanner)
    + UIDatePicker with blocks
    + UIButton with block and custom insets
    + UITextView & UITextField with block
    + [MBProgressHUD](https://github.com/jdg/MBProgressHUD)
    + AlertView Wrapper to support UIAlert view and UIAlertViewController
    + UIColor utilities
    + UIImage utilities
    + UIView utilities for NIB loading & layout constraints
    + ViewController utilities for loading view controller from Storyboard & responding to network notifications

## Requirements

- iOS 9.0+
- Xcode 8+

## Running 
open using cmd line `xed . `
or simply open `OKAppKit.xcodeproj` 
`cmd+r` and we are running. 


## Notes:
This is not maintained since 2016.

## Credits
- [FMDB](https://github.com/ccgus/fmdb)


## Meta

Omkar – [@MAD_Omkar](https://twitter.com/MAD_OmkAR)


[ARomkAR](https://github.com/ARomkAR)

