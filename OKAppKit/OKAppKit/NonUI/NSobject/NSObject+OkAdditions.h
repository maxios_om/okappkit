//
//  NSObject+OkAdditions.h
//  OKWebViewExample
//
//  Created by Omkar khedekar on 14/02/16.
//  Copyright © 2016 omkar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (OkAdditions)

-(id) performSelectorIfResponds:(SEL)aSelector,...;

+(BOOL) isNullOrEmpty:(id)object;

-(BOOL) isNullOrEmpty;
@end
