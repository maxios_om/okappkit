//
//  NSObject+OkAdditions.m
//  OKWebViewExample
//
//  Created by Omkar khedekar on 14/02/16.
//  Copyright © 2016 omkar. All rights reserved.
//

#import "NSObject+OkAdditions.h"
#import <objc/runtime.h>

@implementation NSObject (OkAdditions)

-(id) performSelectorIfResponds:(SEL)aSelector,... {
    
    va_list args;
    
    va_start(args, aSelector);
    
    NSMethodSignature * signature = [self methodSignatureForSelector:aSelector];
    
    NSInvocation * invocation = [NSInvocation invocationWithMethodSignature:signature];
    
    [invocation setTarget:self];
    [invocation setSelector:aSelector];
    
    NSUInteger arg_count = [signature numberOfArguments];
    
    
    for( NSInteger i = 0; i < arg_count - 2; i++ ) {
        
        id arg = va_arg(args, id);
        
        if( [arg isKindOfClass:[NSValue class]] ) {
            
            NSUInteger arg_size;
            
            NSGetSizeAndAlignment([arg objCType], &arg_size, NULL);
            
            void * arg_buffer = malloc(arg_size);
            
            [arg getValue:arg_buffer];
            
            [invocation setArgument:arg_buffer atIndex: 2 + i ];
            
            free(arg_buffer);
            
        }
        else {
            [invocation setArgument:&arg atIndex: 2 + i];
        }
    }
    
    va_end(args);
    
    [invocation invoke];
    
    NSValue    * ret_val  = nil;
    NSUInteger   ret_size = [signature methodReturnLength];
    
    if(  ret_size > 0 ) {
        
        void * ret_buffer = malloc( ret_size );
        
        [invocation getReturnValue:ret_buffer];
        
        ret_val = [NSValue valueWithBytes:ret_buffer
                                 objCType:[signature methodReturnType]];
        
        free(ret_buffer);
    }
    
    return ret_val;
}


+ (void)swizzleClassMethod:(SEL)origSelector withMethod:(SEL)newSelector
{
    Class class = [self class];
    
    Method originalMethod = class_getClassMethod(class, origSelector);
    Method swizzledMethod = class_getClassMethod(class, newSelector);
    
    Class metacls = objc_getMetaClass(NSStringFromClass(class).UTF8String);
    if (class_addMethod(metacls,
                        origSelector,
                        method_getImplementation(swizzledMethod),
                        method_getTypeEncoding(swizzledMethod)) ) {
        /* swizzing super class method, added if not exist */
        class_replaceMethod(metacls,
                            newSelector,
                            method_getImplementation(originalMethod),
                            method_getTypeEncoding(originalMethod));
        
    } else {
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }
}
- (void)swizzleInstanceMethod:(SEL)origSelector withMethod:(SEL)newSelector
{
    Class class = [self class];
    
    Method originalMethod = class_getInstanceMethod(class, origSelector);
    Method swizzledMethod = class_getInstanceMethod(class, newSelector);
    
    if (class_addMethod(class,
                        origSelector,
                        method_getImplementation(swizzledMethod),
                        method_getTypeEncoding(swizzledMethod)) ) {
        /*swizzing super class instance method, added if not exist */
        class_replaceMethod(class,
                            newSelector,
                            method_getImplementation(originalMethod),
                            method_getTypeEncoding(originalMethod));
        
    } else {
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }
}

+(BOOL) isNullOrEmpty:(id)object {
    
    if (!object || [object isKindOfClass:[NSNull class]]) {
        
        return YES;
    }
    
    if ([object isKindOfClass:[NSString class]]) {
        NSString *stringObj =(NSString *) object;
        if (! (stringObj.length> 0)
            ||
            [stringObj isEqualToString:@""]
            ) {
            return YES;
        }
    }
    
    return NO;
}

-(BOOL)isNullOrEmpty {
    
    return [[self class] isNullOrEmpty:self];
}
 
@end
