//
// Copyright (c) 2012 Omkar Khedekar
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
//  NSString+Common.h
//
//  Created by Omkar Khedekar on 24/12/12.
//  Copyright (c) 2012 Omkar Khedekar. All rights reserved.
//

#import <Foundation/Foundation.h>

NSString * EmptyIfNull(NSString *);

@interface NSString (Common) 

+(BOOL) containsCharactersInSet:(NSCharacterSet *)characterSet inputString:(NSString *)inputString;
-(BOOL) isBlank;
-(BOOL) contains:(NSString *)string;
-(BOOL) equalsIgnoreCase:(NSString *) str;
-(BOOL) isEmail;
-(BOOL) isUrl;
-(BOOL) isMobilePhoneNumber;
-(BOOL) isUInteger;
-(BOOL) isInteger;
-(BOOL) containsWhitespace;
-(BOOL) isNumeric;
-(BOOL) isAlpha;
-(BOOL) isAlphaNumeric;
-(BOOL) isAlphaDash;
-(BOOL) isWordMoreThan:(NSInteger)wordCount;
-(BOOL) isWordLessThan:(NSInteger)wordCount;
-(BOOL) isCharMoreThan:(NSInteger)charCount;
-(BOOL) isCharLessThan:(NSInteger)charCount;
-(BOOL) isMatchesRegExp:(NSString *)regExp;


-(NSString *) trim;
-(NSArray *) splitOnChar:(char)ch;
-(NSString *) substringFrom:(NSInteger)from to:(NSInteger)to;
-(NSString *) stringByStrippingWhitespace;
-(NSUInteger) indexOfString: (NSString *) str;
-(NSUInteger) lastIndexOfString:(NSString *) str;
-(NSString *) emojizedString;
+(NSString *) emojizedStringWithString:(NSString *)text;
-(NSInteger) charLength;
-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding;
@end

@interface NSMutableString (Common)
-(BOOL) isBlank;
-(BOOL) contains:(NSString *)string;
@end
