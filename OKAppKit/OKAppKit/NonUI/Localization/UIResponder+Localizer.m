//
//  UIResponder+Localizer.m
//  OKAppKit
//
//  Created by Omkar khedekar on 17/02/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import "UIResponder+Localizer.h"
#import "OKLocalization.h"
#import "NSObject+OkAdditions.h"

//static void *OKLocalizationEnabledKey;

@implementation UIResponder (OKLocalization)

@end

@implementation UIViewController (OKLocalization)
- (void)localize
{
    [[self view] localize];
    [[self navigationItem] localize];
}
@end


@implementation UIView (OKLocalization)
- (void)localize
{
    for(UIView *subview in [self subviews]) {
        if([subview respondsToSelector:@selector(localize)]) {
            [subview localize];
        }
    }
}
@end

@implementation UINavigationItem (OKLocalization)
- (void)localize
{
    [self setTitle:OKLocalisedString([self title])];
    [self setPrompt:OKLocalisedString([self prompt])];
    [[self leftBarButtonItem] localize];
    [[self rightBarButtonItem] localize];
}
@end

@implementation UIBarButtonItem (OKLocalization)
- (void)localize
{
    [self setTitle:OKLocalisedString(self.title)];
}
@end

@implementation UIButton (OKLocalization)
- (void)localize
{
    [self setTitle:OKLocalisedString([self titleForState:UIControlStateNormal]) forState:UIControlStateNormal];
    [self setTitle:OKLocalisedString([self titleForState:UIControlStateHighlighted]) forState:UIControlStateHighlighted];
    [self setTitle:OKLocalisedString([self titleForState:UIControlStateDisabled]) forState:UIControlStateDisabled];
    [self setTitle:OKLocalisedString([self titleForState:UIControlStateSelected]) forState:UIControlStateSelected];
}
@end

@implementation UITextField (OKLocalization)
- (void)localize
{
    self.placeholder       = OKLocalisedString(self.placeholder);
}
@end

@implementation UILabel (OKLocalization)
- (void)localize
{
    if (![self.text isNullOrEmpty]) {
    self.text              = OKLocalisedString(self.text);
    } else {
        /*
    NSAttributedString *as = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    NSMutableString *ms    = [as mutableCopy];
    NSString *src          = ms;
    NSString *dst          = OKLocalisedString(src);
         if (![dst isNullOrEmpty] && ![ms isNullOrEmpty] && ![src isNullOrEmpty] && ) {
         [ms replaceOccurrencesOfString:src
         withString:dst
         options:NSLiteralSearch range:NSMakeRange(0, src.length)];
         }
    self.attributedText    = as;
         */

    }
}
@end

@implementation UITextView (OKLocalization)
- (void)localize
{
    self.text = OKLocalisedString(self.text);
}
@end

@implementation UITableViewCell (OKLocalization)
- (void)localize
{
    [[self textLabel] localize];
    [[self detailTextLabel] localize];

    [[self contentView] localize];
    [[self backgroundView] localize];
    [[self accessoryView] localize];
}
@end

@implementation UICollectionViewCell (OKLocalization)
- (void)localize
{
    [[self contentView] localize];
    [[self backgroundView] localize];
    [[self selectedBackgroundView] localize];
}
@end

