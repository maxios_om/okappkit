//
//  OKLocalization.h
//  OKAppKit
//
//  Created by Omkar khedekar on 16/02/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OKLocalization : NSObject

+(instancetype) shared;

- (NSString *)localizedStringForKey:(NSString *)key;

- (void)setLanguage:(NSString *)language;

@end

NSString* OKLocalisedString(NSString *key);