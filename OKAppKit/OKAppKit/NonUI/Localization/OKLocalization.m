//
//  OKLocalization.m
//  OKAppKit
//
//  Created by Omkar khedekar on 16/02/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import "OKLocalization.h"

static OKLocalization *__sharedInstance = nil;
static NSBundle *bundle = nil;

@implementation OKLocalization

+(instancetype) shared {
    
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[OKLocalization alloc] init];
    });
    
    return __sharedInstance;
}


-(instancetype) init {
    
    if (!__sharedInstance) {
        self = [super init];
        if (self)
        {
            bundle = [NSBundle mainBundle];
        }
        __sharedInstance = self;
    }

    return __sharedInstance;
}

- (NSString *)localizedStringForKey:(NSString *)key
{
    NSString *localisedString = nil;
    if (key) {
        localisedString = [bundle localizedStringForKey:key value:@"" table:nil];
        if (!localisedString) {
            localisedString = key;
        }
    } else {
        
        return key;
    }
    
    return localisedString;
}


//------------------------------------------------------------------------------
// MARK: - Language selection
//------------------------------------------------------------------------------

- (void)setLanguage:(NSString *)language
{
    NSString *path = [[NSBundle mainBundle] pathForResource:language ofType:@"lproj"];
    if (path == nil)
    {
        bundle = [NSBundle mainBundle];
    }
    else
    {
        bundle = [NSBundle bundleWithPath:path];
        
        if (bundle == nil)
        {
            bundle = [NSBundle mainBundle];
        }
    }
}



@end


NSString* OKLocalisedString(NSString *key) {
    
    return [[OKLocalization shared] localizedStringForKey:key];
}