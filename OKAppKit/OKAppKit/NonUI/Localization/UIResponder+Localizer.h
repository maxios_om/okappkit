//
//  UIResponder+Localizer.h
//  OKAppKit
//
//  Created by Omkar khedekar on 17/02/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIResponder (OKLocalization)

@end

@interface UIViewController (OKLocalization)
- (void)localize;
@end

@interface UINavigationItem (OKLocalization)
- (void)localize;
@end

@interface UIBarButtonItem (OKLocalization)
- (void)localize;
@end

@interface UIView (OKLocalization)
- (void)localize;
@end

@interface UIButton (OKLocalization)
- (void)localize;
@end

@interface UITextField (OKLocalization)
- (void)localize;
@end

@interface UILabel (OKLocalization)
- (void)localize;
@end

@interface UITextView (OKLocalization)
- (void)localize;
@end

@interface UITableViewCell (OKLocalization)
- (void)localize;
@end

@interface UICollectionViewCell (OKLocalization)
- (void)localize;
@end
