//
//  NSArray+OK_Utils.h
//  OKAppKit
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

typedef id (^MapBlock)(id object);
typedef BOOL (^TestingBlock)(id object);

@interface NSArray (OK_Utils)
// Random
@property (nonatomic, readonly) id randomItem;
@property (nonatomic, readonly) NSArray *scrambled;

// Utility
@property (nonatomic, readonly) NSArray *reversed;
@property (nonatomic, readonly) NSArray *sorted;
@property (nonatomic, readonly) NSArray *sortedCaseInsensitive;

// Setification
@property (nonatomic, readonly) NSArray *uniqueElements;


// Lisp
@property (nonatomic, readonly) id car;
@property (nonatomic, readonly) NSArray *cdr;

- (NSArray *) unionWithArray: (NSArray *) anArray;
- (NSArray *) intersectionWithArray: (NSArray *) anArray;
- (NSArray *) differenceToArray: (NSArray *) anArray;

- (NSArray *) map: (MapBlock) aBlock;
- (NSArray *) collect: (TestingBlock) aBlock;
- (NSArray *) reject: (TestingBlock) aBlock;

- (id) safeObjectAtIndex: (NSUInteger) index;
- (id)objectAtIndexedSubscript:(NSUInteger)idx;
#if TARGET_OS_IPHONE
- (id) objectForKeyedSubscript: (id) subscript; // for IndexPath - iOS Only
#endif
@end

#pragma mark - Utility
@interface NSMutableArray (OK_Utils)
- (NSMutableArray *) reverseSelf;
- (id) popObject;
- (id) pullObject;
- (NSMutableArray *) pushObject:(id)object;
- (NSMutableArray *) pushObjects:(id)object,...;
@end
