//
//  OK_MACROS.h
//  Proj_DoNotes
//
//  Created by Om's on 24/08/13.
//  Copyright (c) 2013 antom. All rights reserved.
//

#ifndef _OK_MACROS_h
#define _OK_MACROS_h


#define GENERATE_PRAGMA(x) _Pragma(#x)

/* Usage: Use TODO("some message") / FIXME("some message") / NOTE("some message") to generate appropriate warnings */
#define TODO(x) GENERATE_PRAGMA(message("[TODO] " x))
#define FIXME(x) GENERATE_PRAGMA(message("[FIXME] " x))
#define NOTE(x) GENERATE_PRAGMA(message("[NOTE] " x))

#ifndef DebugLog 
#pragma mark -
#pragma mark Logging


#ifdef DEBUG
#define SimpleLog(fmt, ...) NSLog(@" " fmt, ## __VA_ARGS__)
#define DebugLog(fmt, ...) \
        CFShow((__bridge void *)[NSString stringWithFormat:@"%s [LINE: %d] ==>> " fmt,__PRETTY_FUNCTION__, __LINE__, ## __VA_ARGS__]);
#define TRACE(fmt, ...) \
                        CFShow((__bridge void *)[NSString stringWithFormat:@"%s [LINE: %d] ==>> " fmt,__PRETTY_FUNCTION__, __LINE__, ## __VA_ARGS__]);
#else
// do nothing
#define SimpleLog(fmt, ...)
#define DebugLog(fmt, ...)
#define TRACE(fmt, ...)
#endif
#define OK_ERROR(fmt, ...) NSLog(@"=====ERROR==== \n%s:%d (%s): " fmt, __FILE__, __LINE__, __func__, ## __VA_ARGS__)
#endif

#ifdef __cplusplus
#define OK_EXTERN		extern "C" __attribute__((visibility ("default")))
#else
#define OK_EXTERN	        extern __attribute__((visibility ("default")))
#endif

#define OK_STRING_CONST NSString *const
#define OK_EXTERN_STRING_CONST OK_EXTERN NSString *const

#endif //_OK_MACROS_h
