//
//  AbstractRepository.m
//  OKAppKit
//
//  Created by omkar.khedekar on 4/22/15.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import "AbstractRepository.h"
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"
#import "FMDatabaseAdditions.h"

static DBGlobalLoggingState activeState = DBGlobalLoggingStateDisabled;

DBGlobalLoggingState ActiveLggingState() {
    return activeState;
}
DBGlobalLoggingState SetGlobalLoggingState(DBGlobalLoggingState state) {
    activeState = state;
    return ActiveLggingState();
}

@implementation AbstractRepository

+(NSString *)databaseFilename {

    NSString *DBFileName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Application-Database-File"];
    NSAssert((DBFileName!=nil && ![DBFileName isKindOfClass:[NSString class]]), @"Must provide valid database file name in Info.plist against Application-Database-File key");
    return DBFileName;
}


+(NSString *) databasePath {
    NSString *filename = [AbstractRepository databaseFilename];

    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString * dbfilePath = [documentsDirectory stringByAppendingPathComponent:filename];

    return dbfilePath;
}

-(id)init {
    self = [super init];
    
    if (self) {
        NSString *dbPath = [AbstractRepository databasePath];
        db = [FMDatabase databaseWithPath:dbPath];
        queue = [FMDatabaseQueue databaseQueueWithPath:dbPath];
        switch (activeState) {
            case DBGlobalLoggingStateDisabled:
            {
                [db setTraceExecution:NO];
                [db setLogsErrors:NO];
            }
             
                break;
                
            case DBGlobalLoggingStateErrorsOnly:
            {
                [db setTraceExecution:NO];
                [db setLogsErrors:YES];
            }
                
                break;
                
            case DBGlobalLoggingStateVerbose:
            {
                [db setTraceExecution:YES];
                [db setLogsErrors:YES];
            }
                
                break;
            default:
            {
                [db setTraceExecution:NO];
                [db setLogsErrors:NO];
            }
                break;
        }
        
        [db open];
    }

    return self;
}



-(NSInteger) lastInsertRowId {
    NSString * query = @"SELECT last_insert_rowid()";
    NSInteger rowId = [db intForQuery:query];
    return rowId;
}


@end
