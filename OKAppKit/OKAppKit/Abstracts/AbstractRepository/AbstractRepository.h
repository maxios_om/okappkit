//
//  AbstractRepository.h
//  OKAppKit
//
//  Created by omkar.khedekar on 4/22/15.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, DBGlobalLoggingState) {
    DBGlobalLoggingStateDisabled = 0,
    DBGlobalLoggingStateErrorsOnly,
    DBGlobalLoggingStateVerbose = NSUIntegerMax,
};

extern DBGlobalLoggingState ActiveLggingState();
extern DBGlobalLoggingState SetGlobalLoggingState(DBGlobalLoggingState state);

typedef void(^AbstractRepositoryUpdateBlock)(BOOL success);

@protocol AbstractRepositoryProtocol

@optional
/*
  @brief Provides status of record based on record ID
*/

-(BOOL)isRecordExist:(nonnull NSString *)primeryKey;


/*
 @brief inserts new record
 */
-(BOOL) insert:(nonnull __kindof NSObject*) object;


/*
 @brief updates existing record
 */
-(BOOL) update:(nonnull __kindof NSObject*) object;


/*
 @brief inserts new record
 */
-(void) insert:(nonnull __kindof NSObject*) object withComplition:(nullable AbstractRepositoryUpdateBlock) handler;


/*
 @brief updates existing record
 */
-(void) update:(nonnull __kindof NSObject*) object withComplition:(nullable AbstractRepositoryUpdateBlock) handler;



@end

/**
 *   @brief Pure Data access class for models serves as abstration layes for data base intraction
 */

@class FMDatabase;
@class FMDatabaseQueue;
@interface AbstractRepository : NSObject <AbstractRepositoryProtocol> {
    //Protected instance so only subs can use this
    @protected
    FMDatabase *db;
    FMDatabaseQueue *queue;
}


/**
 *   @brief Data base file namme reads data base file name from info.plist data base file name must be proided in info.plist against "Application-Database-File" key
 *
 *   @return name of SQLite DB file
 */
+(nullable NSString *)databaseFilename;


/**
 *   @brief returns last inserted row id helpfull for adding all new objects which dont have any ID assigned
 *
 *  @return last inserted row id
 */
-(NSInteger) lastInsertRowId;



@end
