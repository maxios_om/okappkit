//
//  AbstractViewController.h
//  HadHud
//
//  Created by omkar.khedekar on 3/13/15.
//  Copyright (c) 2015 HadHud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewControllerProtocol.h"
@interface AbstractViewController : UIViewController <AbstractViewControllerProtocol>

@property (nonatomic , strong) IBOutlet UIScrollView *warapperView;;

- (IBAction) dismissAllActiveResponders:(id)sender;

- (BOOL) isInternetRechable;

- (void) checkForNetworkConnectivity;

@end
