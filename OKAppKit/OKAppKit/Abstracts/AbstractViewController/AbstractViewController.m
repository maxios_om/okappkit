//
//  AbstractViewController.m
//  HadHud
//
//  Created by omkar.khedekar on 3/13/15.
//  Copyright (c) 2015 HadHud. All rights reserved.
//

#import "AbstractViewController.h"
#import "UIViewController+Reachability.h"
#import "MBProgressHUD.h"
#import "OKReachability.h"
#import "ALAlertBanner.h"
#import "ALAlertBannerManager.h"

@interface AbstractViewController ()

@property (nonatomic , weak) MBProgressHUD *currentHud;


@end

@implementation AbstractViewController


- (BOOL) isInternetRechable {

    BOOL isReachable = NO;

    NetworkStatus currentReachabilityStatus = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];

    if (NotReachable != currentReachabilityStatus ) {

        isReachable = YES;
    }

    return isReachable;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureUI];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self ok_registerForNetworkReachabilityNotifications];


}

-(void)viewDidDisappear:(BOOL)animated {

    [super viewDidDisappear:animated];
    [self ok_unsubscribeFromNetworkReachabilityNotifications];
}

- (void)checkForNetworkConnectivity {
    [self ok_checkForNetworkConnectivity];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


#pragma mark - overrides 



#pragma mark - Protocall REQs

-(void)configureUI {

    [self localiseUI];
    
}

-(void) localiseUI {
    
}


-(void)showHudWithMessage:(NSString *)message {
    
    [self hideAllHuds];
    
    if (!_currentHud) {
        
        self.currentHud = [MBProgressHUD showHUDAddedTo:[[[UIApplication sharedApplication] delegate] window] animated:YES];
    }
    
    
    [self.currentHud setLabelText:message];
    [self.view bringSubviewToFront:self.currentHud];
}

-(void)hideAllHuds {
    
    [self performSelectorOnMainThread:@selector(hideAllHudsOnMainThread) withObject:nil waitUntilDone:YES];
}

-(void) hideAllHudsOnMainThread {

    [MBProgressHUD hideAllHUDsForView:[[[UIApplication sharedApplication] delegate] window] animated:YES];
    self.currentHud = nil;
}

-(IBAction)dismissAllActiveResponders:(id)sender {
    
    [self resignAllActiveResponders];
}


#pragma mark - Keyboard Management

-(void) resignAllActiveResponders {
    
    [self.view endEditing:YES];
}

-(void) showBannerWithTitle:(NSString *)title message:(NSString *)message image:(UIImage *) image andPosition:(OKMessageNotificationPosition) position{
    
    ALAlertBanner *banner = [ALAlertBanner alertBannerForView:self.view
                                                        style:ALAlertBannerStyleNotify
                                                     position:(ALAlertBannerPosition)position
                                                        title:title
                                                     subtitle:message];
    [banner setSecondsToShow:0];
    [banner show];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [ALAlertBanner hideAlertBannersInView:self.view];
    });
}
@end
