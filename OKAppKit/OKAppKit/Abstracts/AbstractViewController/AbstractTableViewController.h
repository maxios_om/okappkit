//
//  AbstractTableViewController.h
//  OKAppKit
//
//  Created by Omkar khedekar on 21/02/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewControllerProtocol.h"

@interface AbstractTableViewController : UITableViewController <AbstractViewControllerProtocol>

@end
