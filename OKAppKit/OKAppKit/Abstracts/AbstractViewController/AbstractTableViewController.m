//
//  AbstractTableViewController.m
//  OKAppKit
//
//  Created by Omkar khedekar on 21/02/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import "AbstractTableViewController.h"
#import "UIViewController+Reachability.h"
#import "MBProgressHUD.h"
#import "OKReachability.h"
#import "ALAlertBanner.h"
#import "ALAlertBannerManager.h"

@interface AbstractTableViewController ()

@property (nonatomic , weak) MBProgressHUD *currentHud;


@end

@implementation AbstractTableViewController

- (BOOL) isInternetRechable {
    
    BOOL isReachable = NO;
    
    NetworkStatus currentReachabilityStatus = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    
    if (NotReachable != currentReachabilityStatus ) {
        
        isReachable = YES;
    }
    
    return isReachable;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureUI];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self ok_registerForNetworkReachabilityNotifications];
    
    
}

-(void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    [self ok_unsubscribeFromNetworkReachabilityNotifications];
}

- (void)checkForNetworkConnectivity {
    [self ok_checkForNetworkConnectivity];
}

-(void)configureUI {
    
    [self localiseUI];
    
}

-(void) localiseUI {
    
}


-(void)showHudWithMessage:(NSString *)message {
    
    [self hideAllHuds];
    
    if (!_currentHud) {
        
        _currentHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    
    [_currentHud setLabelText:message];
    [self.view bringSubviewToFront:_currentHud];
}

-(void)hideAllHuds {
    
    [self performSelectorOnMainThread:@selector(hideAllHudsOnMainThread) withObject:nil waitUntilDone:YES];
}

-(void) hideAllHudsOnMainThread {
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

-(IBAction)dismissAllActiveResponders:(id)sender {
    
    [self resignAllActiveResponders];
}


#pragma mark - Keyboard Management

-(void) resignAllActiveResponders {
    
    [self.view endEditing:YES];
}

-(void) showBannerWithTitle:(NSString *)title
                    message:(NSString *)message

                      image:(UIImage *) image andPosition:(OKMessageNotificationPosition) position {
    
    
    ALAlertBanner *banner = [ALAlertBanner alertBannerForView:self.view
                                                        style:ALAlertBannerStyleNotify
                                                     position:(ALAlertBannerPosition)position
                                                        title:title
                                                     subtitle:message];
    [banner setSecondsToShow:0];
    [banner show];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [ALAlertBanner hideAlertBannersInView:self.view];
    });
}
@end
