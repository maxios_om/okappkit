//
//  AbstractViewControllerProtocol.h
//  HadHud
//
//  Created by Omkar on 3/26/15.
//  Copyright (c) 2015 HadHud. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, NetworkReachabilityStatus)
{

        NetworkReachabilityStatusNotConnected = 0,
        NetworkReachabilityStatusReachableViaWiFi,
        NetworkReachabilityStatusReachableViaWWAN
};

typedef NS_ENUM(NSInteger, OKMessageNotificationPosition) {
    OKMessageNotificationPositionTop = 0,
    OKMessageNotificationPositionBottom,
    OKMessageNotificationPositionUnderNavBar,
};

@protocol AbstractViewControllerProtocol <NSObject>


@required
/**
 *  <#Description#>
 */
-(void) configureUI;

@optional
/**
 *  <#Description#>
 */
-(void) localiseUI;

/**
 *  <#Description#>
 *
 *  @param message <#message description#>
 */
-(void) showHudWithMessage:(NSString *)message;
/**
 *  <#Description#>
 */
-(void) hideAllHuds;
/**
 *  <#Description#>
 *
 *  @return <#return value description#>
 */
-(BOOL) shouldDisplayReachabilityStatusNotifications;
/**
 *  <#Description#>
 *
 *  @param networkStatus <#networkStatus description#>
 */
-(void) reachabilityDidChanged:(NetworkReachabilityStatus) networkStatus;

/**
 *  <#Description#>
 *
 *  @param title    <#title description#>
 *  @param message  <#message description#>
 *  @param image    <#image description#>
 *  @param position <#position description#>
 */
-(void) showBannerWithTitle:(NSString *)title message:(NSString *)message image:(UIImage *) image andPosition:(OKMessageNotificationPosition) position;

@end

