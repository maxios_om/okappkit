//
//  AbstractModel.h
//  OKAppKit
//
//  Created by Omkar khedekar on 15/01/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 
 @abstract abstraction Layer as protocall
 
 @name AbstractModel
 
*/

@protocol AbstractModel <NSObject>


/**
 *   @brief convniance initiliser method all subclasses should use same
 *
 *  @param json valid JSON dict
 *
 *  @return new instance
 */
+(nullable instancetype) instanceFromJSON:(nullable NSDictionary *)json;


/**
 *   @brief initiliser method all subclasses should use same
 *
 *  @param json valid JSON dict
 *
 *  @return new instance
 */
-(nonnull instancetype) initWithJSONDict:(nullable NSDictionary *)json;


/**
 *   @brief Dictionary representation of model propery names as key and propery as value
 *
 *  @return NSDictionary instance
 */
-(nullable NSDictionary *)dictionaryRepresentation;

@end

/*!
 
 @abstract data base Convenience methodes
 
 @name AbstractModelDBProtocall
 
 */
@protocol AbstractModelDBProtocall <NSObject>

@optional
/**
 *  @brief convniance method that sub classes can write there own Data base update and add logic into
 *
 *  @return <#return value description#>
 */

-(BOOL) saveInDB;


-(void) saveWithComplition:(void (^ __nullable)(BOOL finished))completion;

@end

/*!
 
 @type Abstract Base Class
 
 @name AbstractModel
 
*/
@interface AbstractModel : NSObject <AbstractModel , AbstractModelDBProtocall>

+(nullable instancetype) instanceFromJSON:(nullable NSDictionary *)json;
-(nonnull instancetype) initWithJSONDict:(nullable NSDictionary *)json;
-(nullable NSDictionary *)dictionaryRepresentation;

@end
