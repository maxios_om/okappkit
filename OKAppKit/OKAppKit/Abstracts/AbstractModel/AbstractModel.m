//
//  AbstractModel.m
//  OKAppKit
//
//  Created by Omkar khedekar on 15/01/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import "AbstractModel.h"
#import <objc/runtime.h>

@implementation AbstractModel

-(instancetype) init {
    
    self = [super init];
    if (self) {
        
    }
    return self;
}


-(instancetype) initWithJSONDict:(NSDictionary *)json  {
    
    self = [super init];
    if (self) {
        // Do some thing
    }
    return self;
    
}


+(instancetype) instanceFromJSON:(NSDictionary *)json {
    
    return [[[self class] alloc] initWithJSONDict :json];
}


- (NSDictionary *)dictionaryRepresentation {
    
    unsigned int count = 0;
    
    
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithCapacity:count];
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        NSString *value = [self valueForKey:key];
        
        if (value) {
            if ([value respondsToSelector:@selector(debugDescription)]) {
                [dictionary setObject:[value debugDescription] forKey:key];
            } else {
                [dictionary setObject:value forKey:key];
            }
        }
    }
    free(properties);
    return dictionary;
}

-(NSString *) debugDescription {
    
    NSMutableString * description = [NSMutableString stringWithFormat:@"<Class :  %@ >", NSStringFromClass([self class])];
    
    NSDictionary *dictionaryRepresentation = [self dictionaryRepresentation];
    
    NSArray *allKeys = [dictionaryRepresentation allKeys];
    
    for (NSString *key in allKeys) {
     
        [description appendFormat:@"\n%@ : %@", key , [dictionaryRepresentation valueForKey:key]];
        
    }
    
    [description appendString:@"\t\n"];
    
    return [description copy];
}


-(NSString *) description {
#ifdef DEBUG
    return [self debugDescription];
#else
    return [super description];
#endif
}

@end
