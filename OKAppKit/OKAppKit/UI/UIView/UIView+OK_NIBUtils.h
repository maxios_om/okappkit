//
//  UIView+OK_NIBUtils.h
//  OKAppKit
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (OK_NIBUtils)

+(UINib *) ok_NibFromDefaultBundle:(NSString *)nibName;
+(UINib *) ok_nibForClass:(Class)viewClass;
+(UINib *) ok_nib;
@end
