//
//  UIView+OKConstraindView.m
//  OKAppKit
//
//  Created by Omkar khedekar on 08/05/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import "UIView+OKConstraindView.h"

@interface UIView ()

@end

@implementation UIView (OKConstraindView)

-(void) addConstraindView:(UIView *) subview {

    [subview setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:subview];
    [self setupConstraints:subview];
    [self setNeedsUpdateConstraints];
    
}

-(void) addConstraindView:(UIView *) subview withPadding:(CGFloat) padding {
    
    [subview setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:subview];

    // top constraint
    [self addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                         attribute:NSLayoutAttributeTop
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeTop
                                                        multiplier:1
                                                          constant:padding]];
    
    // Height Leading
    [self addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                         attribute:NSLayoutAttributeLeading
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeLeading
                                                        multiplier:padding
                                                          constant:0.0]];
    
    // Bottom constraint
    [self addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                         attribute:NSLayoutAttributeBottom
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeBottom
                                                        multiplier:(-1 * padding)
                                                          constant:0.0]];
    
    // Trailing constraint
    [self addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                         attribute:NSLayoutAttributeTrailing
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeTrailing
                                                        multiplier:(-1 * padding)
                                                          constant:0.0]];
    
    // Center horizontally
    [self addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.0
                                                          constant:0.0]];
    
    // Center vertically
    [self addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0
                                                          constant:0.0]];
    [self setNeedsUpdateConstraints];
    
}

-(void) setupConstraints:(UIView *) subview {


    
    UIView *mainView = self;
    
    // Width constraint, full of parent view width
    [mainView addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:mainView
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1
                                                           constant:0.0]];
    
    // Height constraint, full of parent view height
    [mainView addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:mainView
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1
                                                           constant:0.0]];
    
    // Center horizontally
    [mainView addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:mainView
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    // Center vertically
    [mainView addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:mainView
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0
                                                           constant:0.0]];
    
}

@end
