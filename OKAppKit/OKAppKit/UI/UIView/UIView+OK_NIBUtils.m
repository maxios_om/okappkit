//
//  UIView+OK_NIBUtils.m
//  OKAppKit
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import "UIView+OK_NIBUtils.h"

@implementation UIView (OK_NIBUtils)

+(UINib *) ok_NibFromDefaultBundle:(NSString *)nibName {
    
    return [UINib nibWithNibName:nibName bundle:nil];
    
}

+(UINib *) ok_nibForClass:(Class)viewClass {
    NSString *classNameForNib = NSStringFromClass(viewClass);
    return [UINib nibWithNibName:classNameForNib bundle:nil];
}

+(UINib *) ok_nib {
    
    return [self ok_nibForClass:[self class]];
}

@end
