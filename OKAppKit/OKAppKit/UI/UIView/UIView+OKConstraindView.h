//
//  UIView+OKConstraindView.h
//  OKAppKit
//
//  Created by Omkar khedekar on 08/05/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (OKConstraindView)

-(void) addConstraindView:(UIView *) subview;

-(void) addConstraindView:(UIView *) subview withPadding:(CGFloat) padding;
@end
