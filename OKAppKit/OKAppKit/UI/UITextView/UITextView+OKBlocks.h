//
//  UITextView+OKBlocks.h
//  OKAppKit
//
//  Created by Omkar khedekar on 05/03/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (OKBlocks)
@property (copy, nonatomic) BOOL (^shouldBegindEditingBlock)(UITextView *textView);
@property (copy, nonatomic) BOOL (^shouldEndEditingBlock)(UITextView *textView);

@property (copy, nonatomic) void (^didBeginEditingBlock)(UITextView *textView);
@property (copy, nonatomic) void (^didEndEditingBlock)(UITextView *textView);

@property (copy, nonatomic) BOOL (^shouldChangeCharactersInRangeBlock)(UITextView *textView, NSRange range, NSString *replacementString);

@property (copy, nonatomic) BOOL (^shouldReturnBlock)(UITextView *textView);
@property (copy, nonatomic) BOOL (^shouldClearBlock)(UITextView *textView);

- (void)setShouldBegindEditingBlock:(BOOL (^)(UITextView *textView))shouldBegindEditingBlock;
- (void)setShouldEndEditingBlock:(BOOL (^)(UITextView *textView))shouldEndEditingBlock;

- (void)setDidBeginEditingBlock:(void (^)(UITextView *textView))didBeginEditingBlock;
- (void)setDidEndEditingBlock:(void (^)(UITextView *textView))didEndEditingBlock;

- (void)setShouldChangeCharactersInRangeBlock:(BOOL (^)(UITextView *textView, NSRange range, NSString *string))shouldChangeCharactersInRangeBlock;

- (void)setShouldClearBlock:(BOOL (^)(UITextView *textView))shouldClearBlock;
- (void)setShouldReturnBlock:(BOOL (^)(UITextView *textView))shouldReturnBlock;
@end
