//
//  AlAlertBannerUIConfigrations.h
//  OKAppKit
//
//  Created by Omkar khedekar on 19/11/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AlAlertBannerUIConfigrations <NSObject>
@optional

@property (nonnull, nonatomic, strong) UIFont *titleLabelFont;

@property (nonnull, nonatomic, strong) UIFont *subTitleLabelFont;

@property (nonnull, nonatomic, strong) UIImage *successStyleImage;

@property (nonnull, nonatomic, strong) UIImage *failureStyleImage;

@property (nonnull, nonatomic, strong) UIImage *notificationStyleImage;

@property (nonnull, nonatomic, strong) UIImage *warningStyleImage;

@end
