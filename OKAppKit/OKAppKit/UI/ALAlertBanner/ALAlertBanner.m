/**
 ALAlertBanner.m

 Created by Anthony Lobianco on 8/12/13.
 Copyright (c) 2013 Anthony Lobianco. All rights reserved.

 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

#if !__has_feature(objc_arc)
#error ALAlertBanner requires that ARC be enabled
#endif

#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_5_0
#error ALAlertBanner requires iOS 5.0 or higher
#endif

#import "ALAlertBanner.h"
#import <QuartzCore/QuartzCore.h>
#import "ALAlertBanner+Private.h"
#import "ALAlertBannerManager.h"
#import "UIImage+OK_Utils.h"

static NSString * const kShowAlertBannerKey = @"showAlertBannerKey";
static NSString * const kHideAlertBannerKey = @"hideAlertBannerKey";
static NSString * const kMoveAlertBannerKey = @"moveAlertBannerKey";

static CGFloat const kMargin = 10.f;
static CGFloat const kNavigationBarHeightDefault = 44.f;
static CGFloat const kNavigationBarHeightiOS7Landscape = 32.f;

static CFTimeInterval const kRotationDurationIphone = 0.3;
static CFTimeInterval const kRotationDurationIPad = 0.4;

static CGFloat const kForceHideAnimationDuration = 0.1f;

#define AL_DEVICE_ANIMATION_DURATION UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? kRotationDurationIPad : kRotationDurationIphone;

//macros referenced from MBProgressHUD. cheers to @matej
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 70000
    #define AL_SINGLELINE_TEXT_HEIGHT(text, font) [text length] > 0 ? [text sizeWithAttributes:nil].height : 0.f;
    #define AL_MULTILINE_TEXT_HEIGHT(text, font, maxSize, mode) [text length] > 0 ? [text boundingRectWithSize:maxSize \
                                                                                                       options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) \
                                                                                                    attributes:nil \
                                                                                                       context:NULL].size.height : 0.f;
#else
    #define AL_SINGLELINE_TEXT_HEIGHT(text, font) [text length] > 0 ? [text sizeWithFont:font].height : 0.f;
    #define AL_MULTILINE_TEXT_HEIGHT(text, font, maxSize, mode) [text length] > 0 ? [text sizeWithFont:font \
                                                                                     constrainedToSize:maxSize \
                                                                                         lineBreakMode:mode].height : 0.f;
#endif

# pragma mark -
# pragma mark Helper Categories


//darkerColor referenced from http://stackoverflow.com/questions/11598043/get-slightly-lighter-and-darker-color-from-uicolor
@implementation UIColor (LightAndDark)

- (UIColor *)darkerColor {
    CGFloat h, s, b, a;
    if ([self getHue:&h saturation:&s brightness:&b alpha:&a])
        return [UIColor colorWithHue:h saturation:s brightness:b * 0.75 alpha:a];
    return nil;
}

@end

@implementation UIDevice (ALSystemVersion)

+ (float)iOSVersion {
    static float version = 0.f;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        version = [[[UIDevice currentDevice] systemVersion] floatValue];
    });
    return version;
}

@end

@implementation UIApplication (ALApplicationBarHeights)

+ (CGFloat)navigationBarHeight {
    //if we're on iOS7 or later, return new landscape navBar height
    if (AL_IOS_7_OR_GREATER && UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]) && [UIDevice currentDevice].userInterfaceIdiom != UIUserInterfaceIdiomPad)
        return kNavigationBarHeightiOS7Landscape;
    
    return kNavigationBarHeightDefault;
}

+ (CGFloat)statusBarHeight {
	return [UIApplication sharedApplication].statusBarFrame.size.height;
}

@end

@interface ALAlertBanner ()<CAAnimationDelegate> {
    @private
    ALAlertBannerManager *manager;
}

@property (nonatomic, assign) ALAlertBannerStyle style;
@property (nonatomic, assign) ALAlertBannerPosition position;
@property (nonatomic, assign) ALAlertBannerState state;
@property (nonatomic) NSTimeInterval fadeOutDuration;
@property (nonatomic, readonly) BOOL isAnimating;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subtitleLabel;
@property (nonatomic, strong) UIImageView *styleImageView;
@property (nonatomic) CGRect parentFrameUponCreation;

@end

@implementation ALAlertBanner

@synthesize titleLabelFont = _titleLabelFont;
@synthesize subTitleLabelFont = _subTitleLabelFont;
@synthesize failureStyleImage = _failureStyleImage;
@synthesize warningStyleImage = _warningStyleImage;
@synthesize notificationStyleImage = _notificationStyleImage;
@synthesize successStyleImage = _successStyleImage;

- (id)init {
    self = [super init];
    if (self) {
        
        [self commonInit];
        
    }
    return self;
}

# pragma mark -
# pragma mark Initializer Helpers

- (void)commonInit {
    self.userInteractionEnabled = YES;
    self.alpha = 0.f;
    self.layer.shadowOpacity = 0.5f;
    self.tag = arc4random_uniform(SHRT_MAX);
    
    [self setupSubviews];
    [self setupInitialValues];
}

-(UIFont *) titleLabelFont {
    
    if (!_titleLabelFont) {
        _titleLabelFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:13.f];
    }
    
    return _titleLabelFont;
}

-(UIFont *) subTitleLabelFont {
    
    if (!_subTitleLabelFont) {
        _subTitleLabelFont = [UIFont fontWithName:@"HelveticaNeue" size:10.f];
    }
    
    return _subTitleLabelFont;
}


-(UIImage *) warningStyleImage {
    
    if (!_warningStyleImage) {
        UIImage *base64ImageData = [UIImage imageWithBase64EncodedString:@"iVBORw0KGgoAAAANSUhEUgAAACgAAAAgCAYAAABgrToAAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowRUJBODUxMzg2MjI2ODExODIyQTk1NTk0NjdBRUI3QyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo4QkJEREE1OTAxMTkxMUUzODI0NkNFOUI3OERGRUNFQyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo4QkJEREE1ODAxMTkxMUUzODI0NkNFOUI3OERGRUNFQyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChNYWNpbnRvc2gpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QTAwQzY0MEZENDI0NjgxMTgyMkE5NTU5NDY3QUVCN0MiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MEVCQTg1MTM4NjIyNjgxMTgyMkE5NTU5NDY3QUVCN0MiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4seh/vAAAGkUlEQVR42sRYW28TRxTey8xeY7CdG9cqIoCLklQRCKl3UUC0UlUSh1CgBAdzTUv6g/pe0YeqVPStD73wCwpC6kP70pS2aiDGSezYsb270++sZ4MTnNbEibrSKJOZ2ZlvvnO+c85aLS3mlHaeRNfOekdVmy8Q4vk5GpPv5J/89a/7a0q7Dx2OlkjE3+1wnW+pxePx99ZdH4Fr8WkfIJ7urs7tuqZNOY4z77huXtfUqa7OZLwps/JC6zK+FQBVTZ/SGV/oiMVmYh0dM5wbT3Vdv9WUvYjBFplsG2BnZ3KfIoLDjm09MQ1jwTCMRcexZ4NADCWTyVTLfrlVAA3OpwEqh7YA1pbRSqZpLpimMQfAt0IcDaJY9bcFFtsC2H9w4G3P93tMy5rjnC9pmlZB8wGyYtl2zvO9eCKRPNVUxVtt4gOHhrhhGlnLNHNgsUjAVFUVaD71OWNly7Jypsknuro6+SoWWzRvWwAB5CMhhAoQJIga8QJgNWqY88jcNuZEIALAmQxBrWWtBaAbAnjg5cE4gJyyTAjDNJcIHDEHwBo1CdZnnNds255F/2QykeheN2BvNkBNZ9M4dAl+No9//WfnrQBUw+ijqhUwPA82oW7+ifqC7G0I4P7UYCrwvQGY7wnTtSoxRcf5vm96nudANDb6RhAEEZOeZdk5jKUQdoZWiWQrTAwmpsHcHExbgFlrBASs8Vqt5pwZG1XHRk5r6HfgeB3LdVrDOVsySUwG/3gF01Yw2J8aPOX5QRzKJbNVCADAGcSeY9tueuSDB+Pj6Qcw6zbf89zIwxhjZOq8HwSxRCLxflPBtAuQwgqeCdsyib1i3eUEmdaoeV5HOj1a7evru/fS3r330qMjZTJ3AOCSRY/EBFHNISSdR562WgXZOoNCyWLPKoUVOjB6H0ewHT095uXMxbvJnt0VapOXLt7txRjAO4EQrB6VVA+KzqFX1nT96qbGQWSMbk3XjkMYOQodkj0SAqtVa7HMpYt/xGKx+xijCSUe334fY396JBbf5/IcnzG9Rnvgsm+BxZ2bJhKYdprp+lOYaZFCB+kP4HRS7NDQgPbh2TNfJnt2ichk6Ac0NjQ4wMGijbVcslhBzl5inNFFp9XNYHB/auAVP/APIlRQxghNW8//QsfB7rUr2fudvXtmGt2J+jRGc1hHAJmom1oPBWNaec/z+5OdySNtA0RFMkXOjVBRBgNVGiNGIAL35Il3vDffeO0bIYLn4hqN0dyJ48d8Wksg6VUKO6h0ihDbY1jmBthWNwywPzUw4iE02DZ8DzfH5gExB7NBK5Z7/Wr2ezCVXyVGGYQli/nr1658B3HYeNEESKOeiXSANOdFEFgIO+kNAUS+tUyDnwN7j+E3ZSoCKI2RT1EIQVBe6N+37wccsm4hSnO0Ziw9UgCNVuSL8GPPMM2SadkIO8Y4spP7wgAB6IZQ1BIuvxJWiD1qO3p7+dXs5B0MVUQjsDWqFKFgdlcuZy59TWGHQhL5Il0UK5ECTcrlZfj2zRcCiBvtwWGvhvmWhaUUVSp0OjHIs9nM7xRWmib+NWqJws5kZuIRmGeymFAAqoqgXYb7zGHhUVjspZYBclQejPE8KuUw34JNcm4BcOzw8HBwJj16RxL032UT5ijsnB0f++rI8DDKQ6oftKgC8uE+BcaNPD60Pm0JIPLtUQDpk/m2So1qPRIIcqmeyUz8gmUz6zG2ytSyTywmunfN0Lt0SVJzVDNCySX64ML4Lojy9ecK48ZfFkjy8K/PEETLruv+TaU8gaN15HulUjm2WCjsXV4u9+CrDXlW6OTzIQI1TK4NCgEINSTAkz69DOXPbotte4SvvoLcl6ICr1arHUulUi9VQdjg+s8Pf1pRHlv1M0YiOY6buI7j5vAhTqUUEogeRH6DWFh1XWcW8l6kckqEsQ3A1BBZyF+DVQKlTiiA4jsFQkN4qWKPsETDI6K1GPeh5tLycqUbe57H0O2mAHHwKAAVdF3zKWZRSUUxLxQJAGFTBWVVEf2SqBekxCwjlRPD0i81+T+TnwJelHzkRxWxo8rAXY8McB2cGeD8Ba/mnV4XIDeM0HvwsiUP0WQO9UEjV2WJ1VAqaQoFbxnfJCPEuBX+JbASqJwncFYElpglK9DlQ0IY86XplaYAb03d/PXz218ki8WigUPi8vaNrGgSRMgMAVCk+RtUHc37yjOzKw0MrxUprdEA1ED6Uy9cOPfbuiLBcwjtGJqj/D8P5fof0R5GA/8IMAB+okNexQrCgAAAAABJRU5ErkJggg=="];
        _warningStyleImage = base64ImageData.copy;
    }
    
    return _warningStyleImage;
}

-(UIImage *) failureStyleImage {
    
    if (!_failureStyleImage) {
        UIImage *base64ImageData = [UIImage imageWithBase64EncodedString:@"iVBORw0KGgoAAAANSUhEUgAAACgAAAAgCAYAAABgrToAAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowRUJBODUxMzg2MjI2ODExODIyQTk1NTk0NjdBRUI3QyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpFQUI4RkMxOEZCOUMxMUUyOTJDMUVBQ0FDMzU1RDQ0RCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpFQUI4RkMxN0ZCOUMxMUUyOTJDMUVBQ0FDMzU1RDQ0RCIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChNYWNpbnRvc2gpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OTYwQzY0MEZENDI0NjgxMTgyMkE5NTU5NDY3QUVCN0MiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MEVCQTg1MTM4NjIyNjgxMTgyMkE5NTU5NDY3QUVCN0MiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7xhUZDAAAHhElEQVR42rSYa2wUVRTH5/3Y3e6WguVNW1sIVD77AeWLyMMYFK0I+plEQSMJBkvpi7YWKIgxMdAWSlR81MRgVFBL8KsS/EDEqNVATAVKS6Flt8t2d3Zn5vo/d6YRsaXTgjeZzN27M/f+7v+ce87ZFRljArV0ckiYSpv2wBzhXtrN61fH/c7MKxCkIJM8uKg8MmdB6fGi0sVrhHtssVhsuWmaX06bll8Q5HkpCJxtOx8pslzMXLe5uGzx6qnC5QPOdZ13MNd8mrMgAKQUDE6aEQqHbhqGnnRdtru4bMmaKcEx9y1d0xKYawhzRu2c/VHpovKCKQESnGM7x2RZmmkYRlLDxLjigLzuTlJJH+5tXdMTmGsIkEO4D8uqmj8RpDQ+nP0hwZmGEVdVNSmJ4gjuUNG4Acg+QDaQkuLEcI86rtuCzQ3ohj6EOeKiJKVxTxi6NsghbfvYeJDKWIOu7RyVZbmQlKOJBFG0JUlKid7dlUwzh8dYJmM1RGMxNpxInGLjwJFZsaEB0zCHsGGLzyUIOSZJDuaW8V7SEoQC27GP4JWKQApu27b1XCQvL6UoSg4TwF2kNHEDkMBEjMdDpolFjatw+npArhLHgnPdvYbO4fpVVbmJzY3gOdqLDFC6O6qipGPRvOHG+tozgRV87dUtnXNmz4nt3tvyEORXMJMEE9sAdGhikSaGuT1oIZdOZ3bGYlEWjydO06oIIcsA1wTleg3DHFZUJU3v0oUm80UYExGDDZheamqoP/vEmlUfBwZEu/Rcxbo2zLJ59959S+FDOvwmx7yo7kqynEVHJH9C/xbG7HTG2h7LjzkYHgFcPcD6TEOPAyDJefCFy5gKSEQrV0Ff03VdaKir+QlwWEvomwygD/lMK+4EWQ62LCYW4ZtJ9LkK6I/AbEooHL4mSnLKsjJbiEXXQ5fhAsNwBa4cYBTRU03CZg28r2qqakK582tWr2wfD24iwH9B7mnZX4aJE4A0AJUGHPklk7wT6cAFUpqqkJpYXRtBMLZ95SRhFM5xwoANQVWlob72l4ngAmWSUciqyu0XAREBJG1Kwl0c9SuMZ2GuFA7WtXA4fAPAGdoAvnPpO4KjjdEBg3J64666P8isM2bO67vnVHc7ZE111QUsbGAx1YdUydxoWVISqo3ArCmcegvjGEJYARTznlUAHgbc76TcdMDB3ML9AuSQ655ee7i2uuovrKQRAI99Xrjwu6JN41xV71BJ/mcbCovwuQsER8q5AeAmC0jt8lNrn+yor9nZqyiqTCYktUBGMZIOOQ61bNHFQxKYuVnRmpsaeletXHEkqHJTBeSQ33ad+tFx7QhWd8ncREGK+d+LvqKITFKW15rptH70vffPFxTODaxc0FP8n1aycMkyURC3hsOhS5FIRPR9TCBfJEUpxlGfeQeD+2o2m5O//+HMK9FoXnY4MXyaTWK9SSlYUrYE6Ys1w2JDiLJJ5qlkk5IIIboPJ43GSc8t4X+amkQFc5O5rDYai64W/w8FAYdi092NAqLfDFHil3OQTUAqNClYk4lzth0R/rEz49kDKlKBEQqFBmk9pMVqQIpQsovdL0BSjkqmkGn0AfA6wgmPbdCHLEBm1Xyzyv7hECit+QUGxUomK0oG7yYoAY2k01RgCKiCuu4ZkCuHkgl5tV9HLQjlbH9RCinkf5pAJsVY2DQZqZiF03H3GX2ODhLugEzj98gNiotQspEg0bqm7IM+3H5NpUraHEQQtuB0tn8ATNw1+F4EfV2W5BkIwsm66ioqq6I2jTuOgcvk2QeboKyCOGRRNY1KZwjgb07080G564FAmY4DkcKEcRSb/DD49RzPrpRbsbiuaJre1FB7YfnyRz6gOTOWtampeU8Rj3fwRL+eZFx573MGteQAuSqKXvx8WCL0XOzuCgzoK3eA4HD6kn6M475GpuKhxfMxiKpEmnbV/fb4isc6Zsya10cnY/DalQ7Av9S8p6UE76TQH017LvMzDizhIi+nMU/SsrLjQo4JuHRp+bwLF/9MUwXMgy0dCKqCvZzKQwglf2zAgFm7kSEOI331k2IUQgB6+Ub/lXbHcVGqtSzGgUniBVrL5ZdIpSGjk88gQIoKYPjnprH8cUwfPPHF8TN1NTvOIn5RyU8n1IQKUfI7XCH085D4IwSH3No23YfzyysERVfA2GUUva3VVZXd8L187qtwB3IJ9GOCZwEGJdnChWXx77q++XwyPtjz4sYNhxBFNu/bf6AcDq/4JTovtVDvyUj85wiOlLszfTFP4lElqVp+GeYuR8zUeHHhWYF+MEklRQtG2lsPnpw9e9Znkz3FPS9sWN9a+cbrvyLuOcybkPK+2Ny4i8Phmf7xEv/tSlY8u65t547KbphRB5w2+khJcfGtjsOtJwDXic+5qaS6no3Pr2/bUbm9G/FPpCrLL9M53EQxlOdoX0lu7h2VPwOSyjS1uGiB1dF+8OvCwsJPx4MLmkl6NqyvaGfM3ZwXybNQCbcGgbtTSd/c/OfDJ52dpW2H3j05EZzgh4ugf78V4bLuhAv69xsvyWRJAOR8dB/G9dX0wrm5wYHeu/799rcAAwCIrBmIih3VZAAAAABJRU5ErkJggg=="];
        _failureStyleImage = base64ImageData.copy;
    }
    
    return _failureStyleImage;
}

-(UIImage *) notificationStyleImage {
    
    if (!_notificationStyleImage) {
        UIImage *base64ImageData = [UIImage imageWithBase64EncodedString:@"iVBORw0KGgoAAAANSUhEUgAAACgAAAAgCAYAAABgrToAAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowRUJBODUxMzg2MjI2ODExODIyQTk1NTk0NjdBRUI3QyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpEQ0ZBNURCN0ZCOUQxMUUyOTJDMUVBQ0FDMzU1RDQ0RCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpEQ0ZBNURCNkZCOUQxMUUyOTJDMUVBQ0FDMzU1RDQ0RCIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChNYWNpbnRvc2gpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OTcwQzY0MEZENDI0NjgxMTgyMkE5NTU5NDY3QUVCN0MiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MEVCQTg1MTM4NjIyNjgxMTgyMkE5NTU5NDY3QUVCN0MiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4ZtTuVAAAEPklEQVR42sxYbWxTVRi+5360996OrGtg3egK026FdpSunYLMfagRWJQsLhLmhIGaqCTGBI0fMUZFHBGUxF/GxciMvwwhZkb9I0Hhx+KMDiFoUEEJ6gwawkDopb29957jc2rhhz+MacttT3JyT09PTp77vO/7vO97CWNM+K+RvTwnXI+hzQv8r3OiUONDrvSFN0bj9ZZlveI4tBvW8UiieNTj9Tx1+uSJc6XcV1EGA40h4tj2hEjEWzRNy/p0fU6SpbjAhD01waDfX99HKW0BuPOyJDnYooqj5E3TjJZ6Z0UZbItEYh6Px1BkOS8QwjAJgObrfLpREwD7+3pnJUmifE0EwcG0oREknUqdrQmAzz/3zPTAmtW/AZRUnEqqM3lxbMf2faXeSa6DDvbOzBy58/sffvRFo+2ZlStunsHep5hWKTpYUYCBxoWkwd/Q8sae3XRwcJ2GrQuY58sR6rIBtkaWeCkTHrJte50gsHDRbagsydOKIj/+88kT+XIAliUzrW2xWy3b3imKYr2qerOI2DlErsMok8x8vgtBPIpje6uSSRZHlo4yx9kWDof/yOVyppXPm7Ism7CISBnziiKxFzY3J8oNPLFE5kYZpds2b9n0zZdTh/elU52/Q16ugD0TrJkiIVSE3IRaWjKu5+Ib2mKrKXWeGLlvw7Gxl18ajyfSx5HOhvHXZQCzwaCOdcGxk8nEt9Vg8IU7br/t1O5Xx8axPmI79lAum5UAzhEYI2CRZxEr4Pdbj2195LDrDD779JPTWzZv+gzLr/hvVVWHbMsyuInBnsJVAeYn6a7UrK7rf7oOcOujD7+Fxy98HVnSEcGjCUAKqQz+ZwuUelEw+Hq6u9+vRHYqxcRnrvqYrukPIHIvoTjg7PHURjjORYvC2eEN6w9WC6BQZE+ybGsVL6u4tHD2wJzXdhy1szP5E45crCpARMMgsodQkJdrewJFscoG1q75qFIFSMkAdZ++HqblwWEXar9/zM6WdcSy/b09X1cVYCQaDxqG0Spz3+OxgUoFZubm1ZLJ5cdxJFfQzPa4thi52nWAqqbdD+YMIooU4BhvjniBoGuaMLpxZLL4EgFkv09kWXnQdYAwbT8AOlycCwwiQBxKtZu60hdCodApgKsDmxPoUdR33xk/5irA9liiIZMxAggGblaF+x2KA49jO1pzc9N3bUuX6Qie98BccMf2Fw/09fYcdRXgvLq6FbxSkSSRB4WErKEXMojApP0fTK5CTzypamrwtV1jh+4duudtnDFdzSTBYGMkY1wpCDWY04pVr4crdC6bawoGg+bru3YeQAM1ga2zrkfx8kTiHBi0rnZu1/xSUcjddw2c+Xhy/4cA9ya2Zqvy6QNVzBTkZe3Bzw8tgK+RBfPnG6gH/9o4MvxrR0d8Cke++HeDVM4otSdZidlVfEHeGJ3G5LXfpUp/3SK1/vntbwEGAP8IriqCAm7mAAAAAElFTkSuQmCC"];
        _notificationStyleImage = base64ImageData.copy;
    }
    
    return _notificationStyleImage;
}

-(UIImage *) successStyleImage {
    
    if (!_successStyleImage) {
        UIImage *base64ImageData = [UIImage imageWithBase64EncodedString:@"iVBORw0KGgoAAAANSUhEUgAAACgAAAAgCAYAAABgrToAAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowRUJBODUxMzg2MjI2ODExODIyQTk1NTk0NjdBRUI3QyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpFQUI4RkMxNEZCOUMxMUUyOTJDMUVBQ0FDMzU1RDQ0RCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpFQUI4RkMxM0ZCOUMxMUUyOTJDMUVBQ0FDMzU1RDQ0RCIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChNYWNpbnRvc2gpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OTYwQzY0MEZENDI0NjgxMTgyMkE5NTU5NDY3QUVCN0MiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MEVCQTg1MTM4NjIyNjgxMTgyMkE5NTU5NDY3QUVCN0MiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6CgmgpAAAGbUlEQVR42syY629URRjGz8y57q0XCoHYhkTwoxQQEaxgAIugIBW/kEIUkVi5hCjKWgrBC6bQAoWW1l4AjYiX+A/4XRIjoFgR9ZsSoRe2dGm7bLu73d0zPu85AyIW2lJKOclm5txmfvO8tznLTNNUxupg+JmmUZy27Zc0TXul7eJfsVuf4WMKZ5nFQmFBVVUfFrb4/KHJUzwPBCDBWZa5hilsGxTsMQ2zm3OepwjleO7kqb4xBXTNar5sC+UtXdc7NU3vQBsGaCfnLNcWjpL+MQGUcGuFIjZZptGu6VpY5awXl+Mwc49hGFdxPkkI5YvrkPw+w70mFKUEJm0n5VSuhnE5xpjShyfSgEzquhHljE3E80fpPe0+wr0OuPVQrgVwIVXlCSGEzRjT0RoKHBBHiiADAX9ky+ZNP98XQBmtJTDbWgnXhYBI43IScGBzHkP0MhuQHMGT+ujD986tfLHo61EHlHBvAOJVwLVK5ZKgSpFJ6TbMq+E++iITKuvluz84X1T0QgPutY4qoITbjMlXS7hOwPXjcgJ3VfJ/3IN+wgCwH3DWnvLdv65YvqzxOtyAQQKJTV3XvsGgxWxkcFsw/RqMc4ngYNZrFBCKdDYcBj1GcJblMSsrypsB13Az3P8APR7Lg9dPwFEfwTSllOmHCwmTEdybGGe1KX0OyvXB3/qhmC7ByPmgnJ3pAd3+yj1/LHtuKSnXdut4N0zs9Xh8SJLHKaMjcXahrySTya1QlMXjia/EUOFMcyu6qwD3N+C6ARfBUMgcQsN9WwKSMFkQxNpXsfe3pUsWk3KhgcbUpHJ+rOgzjJKLgbFiNY5zgfn6cbwNRdREPHFCDA73DrorDd3xuR4sNoJzpBJSzIGyYGFKKx6v16vuq9hzfsmzhY23g3MACY7MChNMcuG0HgySxOApKEnOnMCxGZNztMfF7eAMI4juCsMwQ4aukc/13uRvaTxjumZVDK/H6zuwr+Lc4sJFTbjXcSercAzxKWrgBJSZCJQjJ44DltoYrkcB2Q0ztyDgNlCZYgPDvYteESpEm65plxlzlEM6cZQjK1mkGlrd7/daVQcqfyG4nIl5HYO5DS8rC571+/1RTVPlXkzQgLabp1gffKibHB3BfQlyrAPk+n9fhxyGUYrnnocbXMCeLoxFxSggJDySsaK6lUIxYdbAwQP7fy98ZmHTeMDZtj2oX/MtmzZ+ubd891lMrLqmYEkMncLAQmYMFHJ+FUq2A/IiLq5C6lhHJQCpKwi/LQTcRU3VKM9F3AB1TEk+50XfR6nE5/MFaqurmhcueLoRyl1JDwHO8cHxk/JaOi+31HOubti56/3p8DOYmNIBKeFMQj+bfAoK2fDyVDyRWAnYfM6UqYD8E3mzGwtj7sLgDIIiliERC4t8LiMQMKsPVTXPn1dAZg3bQ4RzAO20reCl1nCopREQJWU7dwGy33b9m0UZk7bECdwgChNSTotiEh989gK5Bt6j8kXvWG5mcDcAgLP8fl9GTXXVmXlPFRzBPFeHA+cAOmGGl6BkK5Rs4iov2V62a1o8TrGCjbhwzB11i7nSDxjyu6Rtp1WoniJ4HOoNr3TLP537A1CurubgmYKCuXcFdyMP0ohSyTZSUmV8Y3D7jnxAcglnoe3F3Nzxffgp5Uq3IiiOOd11OvcDFK0ZgQyr9vChH5+c+wTBdd0N3H8qCUEKUnJi3uXOUEs9MDYGS8sepSQoVUm5SUWkcKZStBMorhhSuQCZmGInKzvTqqs+dHrOnNnHRgI34G4m7UKGANkAgA3B0h3TYrG+Xnm71w0c0S/X5JEpxJA+58nKzjLrD1f/NHv2LMDldkPVEe2KBtxuESRWHroeONuC22f0xWKC6ilFN0WmC0g+SptNx7SZ2dlZRn1dzanHZz1GcD0jhbvjN4ktlaSMj8zfjAqAfKZQzcbHDFUFQTkuk/rUQjlvU33tD4A7eq/gBv1okubuoMxftb/yHCqBz/U1JRNtBkUq2uxxOTmepo9rT86cOYOUi9wruCF91UnIK4sWLmisq6k6jbJoAgDKKRnU5uTkcCj3PeA+uddwQ/7slD7ZOX/+vKbamoOn8NWVpBQzYcL4+JGGuu9mTM8/Nhpww/outl3IMCoCQZ6ZOmVK4uiR+pP5+dNIuWujAedsOIb77xZVEkT3OHQL8fsWcNGRwHVdabszYF8krDzIxz8CDACkO+4g+CdLKgAAAABJRU5ErkJggg=="];
        _successStyleImage = base64ImageData.copy;
    }
    
    return _successStyleImage;
}

- (void)setupInitialValues {
    _fadeOutDuration = 0.2;
    _showAnimationDuration = 0.25;
    _hideAnimationDuration = 0.2;
    _isScheduledToHide = NO;
    _bannerOpacity = 0.93f;
    _secondsToShow = 3.5;
    _allowTapToDismiss = YES;
    _shouldForceHide = NO;
    
    manager = [ALAlertBannerManager sharedManager];
    self.delegate = (ALAlertBannerManager <ALAlertBannerViewDelegate> *)manager;
}

- (void)setupSubviews {
    _styleImageView = [[UIImageView alloc] init];
    [self addSubview:_styleImageView];
    
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.font = self.titleLabelFont;
    _titleLabel.textColor = [UIColor colorWithWhite:1.f alpha:0.9f];
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    _titleLabel.numberOfLines = 1;
    _titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    _titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    _titleLabel.layer.shadowOffset = CGSizeMake(0.f, -1.f);
    _titleLabel.layer.shadowOpacity = 0.3f;
    _titleLabel.layer.shadowRadius = 0.f;
    [self addSubview:_titleLabel];
    
    _subtitleLabel = [[UILabel alloc] init];
    _subtitleLabel.backgroundColor = [UIColor clearColor];
    _subtitleLabel.font = self.subTitleLabelFont;
    _subtitleLabel.textColor = [UIColor colorWithWhite:1.f alpha:0.9f];
    _subtitleLabel.textAlignment = NSTextAlignmentLeft;
    _subtitleLabel.numberOfLines = 0;
    _subtitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _subtitleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    _subtitleLabel.layer.shadowOffset = CGSizeMake(0.f, -1.f);
    _subtitleLabel.layer.shadowOpacity = 0.3f;
    _subtitleLabel.layer.shadowRadius = 0.f;
    [self addSubview:_subtitleLabel];
}

# pragma mark -
# pragma mark Custom Setters & Getters

-(void)setStyle:(ALAlertBannerStyle)style {
    _style = style;
    
    switch (style) {
        case ALAlertBannerStyleSuccess:
        {
            self.styleImageView.image = self.successStyleImage;
        }
            break;
            
        case ALAlertBannerStyleFailure:
            self.styleImageView.image = self.failureStyleImage;
            break;
            
        case ALAlertBannerStyleNotify:
            self.styleImageView.image = self.notificationStyleImage;
            break;
            
        case ALAlertBannerStyleWarning:
        {
            
            self.styleImageView.image = self.warningStyleImage;
            
            //tone the shadows down a little for the yellow background
            self.titleLabel.layer.shadowOpacity = 0.2f;
            self.subtitleLabel.layer.shadowOpacity = 0.2f;
        }
            break;
    }
}

- (void)setShowShadow:(BOOL)showShadow {
    _showShadow = showShadow;
    
    CGFloat oldShadowRadius = self.layer.shadowRadius;
    CGFloat newShadowRadius;
    
    if (showShadow) {
        newShadowRadius = 3.f;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOffset = CGSizeMake(0.f, self.position == ALAlertBannerPositionBottom ? -1.f : 1.f);
        CGRect shadowPath = CGRectMake(self.bounds.origin.x - kMargin, self.bounds.origin.y, self.bounds.size.width + kMargin*2.f, self.bounds.size.height);
        self.layer.shadowPath = [UIBezierPath bezierPathWithRect:shadowPath].CGPath;
        
        self.fadeInDuration = 0.15f;
    }
    
    else {
        newShadowRadius = 0.f;
        self.layer.shadowRadius = 0.f;
        self.layer.shadowOffset = CGSizeZero;
        
        //if on iOS7, keep fade in duration at a value greater than 0 so it doesn't instantly appear behind the translucent nav bar
        self.fadeInDuration = (AL_IOS_7_OR_GREATER && self.position == ALAlertBannerPositionTop) ? 0.15f : 0.f;
    }
    
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.layer.shadowRadius = newShadowRadius;
    
    CABasicAnimation *fadeShadow = [CABasicAnimation animationWithKeyPath:@"shadowRadius"];
    fadeShadow.fromValue = [NSNumber numberWithFloat:oldShadowRadius];
    fadeShadow.toValue = [NSNumber numberWithFloat:newShadowRadius];
    fadeShadow.duration = self.fadeOutDuration;
    [self.layer addAnimation:fadeShadow forKey:@"shadowRadius"];
}

- (void)setAllowTapToDismiss:(BOOL)allowTapToDismiss {
    if (self.tappedBlock && allowTapToDismiss) {
        NSLog(@"allowTapToDismiss should be set to NO when a tappedBlock is used. If you want to reinstate the tap to dismiss behavior, call [alertBanner hide] in tappedBlock.");
        return;
    }
    _allowTapToDismiss = allowTapToDismiss;
}

- (BOOL)isAnimating {
    return (self.state == ALAlertBannerStateShowing ||
            self.state == ALAlertBannerStateHiding ||
            self.state == ALAlertBannerStateMovingForward ||
            self.state == ALAlertBannerStateMovingBackward);
}

# pragma mark -
# pragma mark Public Class Methods

+ (NSArray *)alertBannersInView:(UIView *)view {
    return [[ALAlertBannerManager sharedManager] alertBannersInView:view];
}

+ (void)hideAllAlertBanners {
    [[ALAlertBannerManager sharedManager] hideAllAlertBanners];
}

+ (void)hideAlertBannersInView:(UIView *)view {
    [[ALAlertBannerManager sharedManager] hideAlertBannersInView:view];
}

+ (void)forceHideAllAlertBannersInView:(UIView *)view {
    [[ALAlertBannerManager sharedManager] forceHideAllAlertBannersInView:view];
}

+ (ALAlertBanner *)alertBannerForView:(UIView *)view style:(ALAlertBannerStyle)style position:(ALAlertBannerPosition)position title:(NSString *)title {
    return [self alertBannerForView:view style:style position:position title:title subtitle:nil tappedBlock:nil];
}

+ (ALAlertBanner *)alertBannerForView:(UIView *)view style:(ALAlertBannerStyle)style position:(ALAlertBannerPosition)position title:(NSString *)title subtitle:(NSString *)subtitle {
    return [self alertBannerForView:view style:style position:position title:title subtitle:subtitle tappedBlock:nil];
}

+ (ALAlertBanner *)alertBannerForView:(UIView *)view style:(ALAlertBannerStyle)style position:(ALAlertBannerPosition)position title:(NSString *)title subtitle:(NSString *)subtitle tappedBlock:(void (^)(ALAlertBanner *alertBanner))tappedBlock {
    ALAlertBanner *alertBanner = [ALAlertBanner createAlertBannerForView:view style:style position:position title:title subtitle:subtitle];
    alertBanner.allowTapToDismiss = tappedBlock ? NO : alertBanner.allowTapToDismiss;
    alertBanner.tappedBlock = tappedBlock;
    return alertBanner;
}

# pragma mark -
# pragma mark Internal Class Methods

+ (ALAlertBanner *)createAlertBannerForView:(UIView *)view style:(ALAlertBannerStyle)style position:(ALAlertBannerPosition)position title:(NSString *)title subtitle:(NSString *)subtitle {
    ALAlertBanner *alertBanner = [[ALAlertBanner alloc] init];
    
    if (![view isKindOfClass:[UIWindow class]] && position == ALAlertBannerPositionUnderNavBar)
        [[NSException exceptionWithName:@"Wrong ALAlertBannerPosition For View Type" reason:@"ALAlertBannerPositionUnderNavBar should only be used if you are presenting the alert banner on the AppDelegate window. Use ALAlertBannerPositionTop or ALAlertBannerPositionBottom for normal UIViews" userInfo:nil] raise];
    
    alertBanner.titleLabel.text = !title ? @" " : title;
    alertBanner.subtitleLabel.text = subtitle;
    alertBanner.style = style;
    alertBanner.position = position;
    alertBanner.state = ALAlertBannerStateHidden;
    
    [view addSubview:alertBanner];
    
    [alertBanner setInitialLayout];
    [alertBanner updateSizeAndSubviewsAnimated:NO];
    
    return alertBanner;
}

# pragma mark -
# pragma mark Public Instance Methods

- (void)show {
    [self.delegate showAlertBanner:self hideAfter:self.secondsToShow];
}

- (void)hide {
    [self.delegate hideAlertBanner:self forced:NO];
}

# pragma mark -
# pragma mark Internal Instance Methods

- (void)showAlertBanner {
    if (!CGRectEqualToRect(self.parentFrameUponCreation, self.superview.bounds)) {
        //if view size changed since this banner was created, reset layout
        [self setInitialLayout];
        [self updateSizeAndSubviewsAnimated:NO];
    }
    
    [self.delegate alertBannerWillShow:self inView:self.superview];
    
    self.state = ALAlertBannerStateShowing;
    
    double delayInSeconds = self.fadeInDuration;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if (self.position == ALAlertBannerPositionUnderNavBar) {
            //animate mask
            CGPoint currentPoint = self.layer.mask.position;
            CGPoint newPoint = CGPointMake(0.f, -self.frame.size.height);
            
            self.layer.mask.position = newPoint;
            
            CABasicAnimation *moveMaskUp = [CABasicAnimation animationWithKeyPath:@"position"];
            moveMaskUp.fromValue = [NSValue valueWithCGPoint:currentPoint];
            moveMaskUp.toValue = [NSValue valueWithCGPoint:newPoint];
            moveMaskUp.duration = self.showAnimationDuration;
            moveMaskUp.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
            
            [self.layer.mask addAnimation:moveMaskUp forKey:@"position"];
        }
        
        CGPoint oldPoint = self.layer.position;
        CGFloat yCoord = oldPoint.y;
        switch (self.position) {
            case ALAlertBannerPositionTop:
            case ALAlertBannerPositionUnderNavBar:
                yCoord += self.frame.size.height;
                break;
            case ALAlertBannerPositionBottom:
                yCoord -= self.frame.size.height;
                break;
        }
        CGPoint newPoint = CGPointMake(oldPoint.x, yCoord);
        
        self.layer.position = newPoint;
        
        CABasicAnimation *moveLayer = [CABasicAnimation animationWithKeyPath:@"position"];
        moveLayer.fromValue = [NSValue valueWithCGPoint:oldPoint];
        moveLayer.toValue = [NSValue valueWithCGPoint:newPoint];
        moveLayer.duration = self.showAnimationDuration;
        moveLayer.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        moveLayer.delegate = self;
        [moveLayer setValue:kShowAlertBannerKey forKey:@"anim"];
        
        [self.layer addAnimation:moveLayer forKey:kShowAlertBannerKey];
    });
    
    [UIView animateWithDuration:self.fadeInDuration delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.alpha = self.bannerOpacity;
    } completion:nil];
}

- (void)hideAlertBanner {
    [self.delegate alertBannerWillHide:self inView:self.superview];
    
    self.state = ALAlertBannerStateHiding;
    
    if (self.position == ALAlertBannerPositionUnderNavBar) {
        CGPoint currentPoint = self.layer.mask.position;
        CGPoint newPoint = CGPointZero;
        
        self.layer.mask.position = newPoint;
        
        CABasicAnimation *moveMaskDown = [CABasicAnimation animationWithKeyPath:@"position"];
        moveMaskDown.fromValue = [NSValue valueWithCGPoint:currentPoint];
        moveMaskDown.toValue = [NSValue valueWithCGPoint:newPoint];
        moveMaskDown.duration = self.shouldForceHide ? kForceHideAnimationDuration : self.hideAnimationDuration;
        moveMaskDown.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
        
        [self.layer.mask addAnimation:moveMaskDown forKey:@"position"];
    }
    
    CGPoint oldPoint = self.layer.position;
    CGFloat yCoord = oldPoint.y;
    switch (self.position) {
        case ALAlertBannerPositionTop:
        case ALAlertBannerPositionUnderNavBar:
            yCoord -= self.frame.size.height;
            break;
        case ALAlertBannerPositionBottom:
            yCoord += self.frame.size.height;
            break;
    }
    CGPoint newPoint = CGPointMake(oldPoint.x, yCoord);
    
    self.layer.position = newPoint;
    
    CABasicAnimation *moveLayer = [CABasicAnimation animationWithKeyPath:@"position"];
    moveLayer.fromValue = [NSValue valueWithCGPoint:oldPoint];
    moveLayer.toValue = [NSValue valueWithCGPoint:newPoint];
    moveLayer.duration = self.shouldForceHide ? kForceHideAnimationDuration : self.hideAnimationDuration;
    moveLayer.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    moveLayer.delegate = self;
    [moveLayer setValue:kHideAlertBannerKey forKey:@"anim"];
    
    [self.layer addAnimation:moveLayer forKey:kHideAlertBannerKey];
}

- (void)pushAlertBanner:(CGFloat)distance forward:(BOOL)forward delay:(double)delay {
    self.state = (forward ? ALAlertBannerStateMovingForward : ALAlertBannerStateMovingBackward);
    
    CGFloat distanceToPush = distance;
    if (self.position == ALAlertBannerPositionBottom)
        distanceToPush *= -1;
    
    CALayer *activeLayer = self.isAnimating ? (CALayer *)[self.layer presentationLayer] : self.layer;
    
    CGPoint oldPoint = activeLayer.position;
    CGPoint newPoint = CGPointMake(oldPoint.x, (self.layer.position.y - oldPoint.y)+oldPoint.y+distanceToPush);
    
    double delayInSeconds = delay;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self.layer.position = newPoint;
        
        CABasicAnimation *moveLayer = [CABasicAnimation animationWithKeyPath:@"position"];
        moveLayer.fromValue = [NSValue valueWithCGPoint:oldPoint];
        moveLayer.toValue = [NSValue valueWithCGPoint:newPoint];
        moveLayer.duration = forward ? self.showAnimationDuration : self.hideAnimationDuration;
        moveLayer.timingFunction = forward ? [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut] : [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
        moveLayer.delegate = self;
        [moveLayer setValue:kMoveAlertBannerKey forKey:@"anim"];
        
        [self.layer addAnimation:moveLayer forKey:kMoveAlertBannerKey];
    });
}

# pragma mark -
# pragma mark Touch Recognition

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.state != ALAlertBannerStateVisible)
        return;
    
    if (self.tappedBlock) // && !self.isScheduledToHide ...?
        self.tappedBlock(self);
    
    if (self.allowTapToDismiss)
        [self.delegate hideAlertBanner:self forced:NO];
}

# pragma mark -
# pragma mark Private Methods

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {    
    if ([[anim valueForKey:@"anim"] isEqualToString:kShowAlertBannerKey] && flag) {
        [self.delegate alertBannerDidShow:self inView:self.superview];
        self.state = ALAlertBannerStateVisible;
    }
    
    else if ([[anim valueForKey:@"anim"] isEqualToString:kHideAlertBannerKey] && flag) {
        [UIView animateWithDuration:self.shouldForceHide ? kForceHideAnimationDuration : self.fadeOutDuration delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
            self.alpha = 0.f;
        } completion:^(BOOL finished) {
            self.state = ALAlertBannerStateHidden;
            [self.delegate alertBannerDidHide:self inView:self.superview];
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            [self removeFromSuperview];
        }];
    }
    
    else if ([[anim valueForKey:@"anim"] isEqualToString:kMoveAlertBannerKey] && flag) {
        self.state = ALAlertBannerStateVisible;
    }
}

- (void)setInitialLayout {
    self.layer.anchorPoint = CGPointMake(0.f, 0.f);
    
    UIView *superview = self.superview;
    self.parentFrameUponCreation = superview.bounds;
    BOOL isSuperviewKindOfWindow = ([superview isKindOfClass:[UIWindow class]]);
    
    CGSize maxLabelSize = CGSizeMake(superview.bounds.size.width - (kMargin*3) - self.styleImageView.image.size.width, CGFLOAT_MAX);
    CGFloat titleLabelHeight = AL_SINGLELINE_TEXT_HEIGHT(self.titleLabel.text, self.titleLabel.font);
    CGFloat subtitleLabelHeight = AL_MULTILINE_TEXT_HEIGHT(self.subtitleLabel.text, self.subtitleLabel.font, maxLabelSize, self.subtitleLabel.lineBreakMode);
    CGFloat heightForSelf = titleLabelHeight + subtitleLabelHeight + (self.subtitleLabel.text == nil || self.titleLabel.text == nil ? kMargin*2 : kMargin*2.5);
    
    CGRect frame = CGRectMake(0.f, 0.f, superview.bounds.size.width, heightForSelf);
    CGFloat initialYCoord = 0.f;
    switch (self.position) {
        case ALAlertBannerPositionTop:
            initialYCoord = -heightForSelf;
            if (isSuperviewKindOfWindow) initialYCoord += [UIApplication statusBarHeight];
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
            if (AL_IOS_7_OR_GREATER) {
                id nextResponder = [self nextAvailableViewController:self];
                if (nextResponder) {
                    UIViewController *vc = nextResponder;
                    if (!(vc.automaticallyAdjustsScrollViewInsets && [vc.view isKindOfClass:[UIScrollView class]])) {
                        initialYCoord += [vc topLayoutGuide].length;
                    }
                }
            }
#endif
            break;
        case ALAlertBannerPositionBottom:
            initialYCoord = superview.bounds.size.height;
            break;
        case ALAlertBannerPositionUnderNavBar:
            initialYCoord = -heightForSelf + [UIApplication navigationBarHeight] + [UIApplication statusBarHeight];
            break;
    }
    frame.origin.y = initialYCoord;
    self.frame = frame;
    
    //if position is under the nav bar, add a mask
    if (self.position == ALAlertBannerPositionUnderNavBar) {
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        CGRect maskRect = CGRectMake(0.f, frame.size.height, frame.size.width, superview.bounds.size.height); //give the mask enough height so it doesn't clip the shadow
        CGPathRef path = CGPathCreateWithRect(maskRect, NULL);
        maskLayer.path = path;
        CGPathRelease(path);
        
        self.layer.mask = maskLayer;
        self.layer.mask.position = CGPointZero;
    }
}

- (void)updateSizeAndSubviewsAnimated:(BOOL)animated {
    CGSize maxLabelSize = CGSizeMake(self.superview.bounds.size.width - (kMargin*3.f) - self.styleImageView.image.size.width, CGFLOAT_MAX);
    CGFloat titleLabelHeight = AL_SINGLELINE_TEXT_HEIGHT(self.titleLabel.text, self.titleLabel.font);
    CGFloat subtitleLabelHeight = AL_MULTILINE_TEXT_HEIGHT(self.subtitleLabel.text, self.subtitleLabel.font, maxLabelSize, self.subtitleLabel.lineBreakMode);
    CGFloat heightForSelf = titleLabelHeight + subtitleLabelHeight + (self.subtitleLabel.text == nil || self.titleLabel.text == nil ? kMargin*2.f : kMargin*2.5f);
    
    CFTimeInterval boundsAnimationDuration = AL_DEVICE_ANIMATION_DURATION;
        
    CGRect oldBounds = self.layer.bounds;
    CGRect newBounds = oldBounds;
    newBounds.size = CGSizeMake(self.superview.frame.size.width, heightForSelf);
    self.layer.bounds = newBounds;
    
    if (animated) {
        CABasicAnimation *boundsAnimation = [CABasicAnimation animationWithKeyPath:@"bounds"];
        boundsAnimation.fromValue = [NSValue valueWithCGRect:oldBounds];
        boundsAnimation.toValue = [NSValue valueWithCGRect:newBounds];
        boundsAnimation.duration = boundsAnimationDuration;
        [self.layer addAnimation:boundsAnimation forKey:@"bounds"];
    }
    
    if (animated) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:boundsAnimationDuration];
    }
    
    self.styleImageView.frame = CGRectMake(kMargin, (self.frame.size.height/2.f) - (self.styleImageView.image.size.height/2.f), self.styleImageView.image.size.width, self.styleImageView.image.size.height);
    self.titleLabel.frame = CGRectMake(self.styleImageView.frame.origin.x + self.styleImageView.frame.size.width + kMargin, kMargin, maxLabelSize.width, titleLabelHeight);
    self.subtitleLabel.frame = CGRectMake(self.titleLabel.frame.origin.x, self.titleLabel.frame.origin.y + self.titleLabel.frame.size.height + (self.titleLabel.text == nil ? 0.f : kMargin/2.f), maxLabelSize.width, subtitleLabelHeight);
    
    if (animated) {
        [UIView commitAnimations];
    }
    
    if (self.showShadow) {
        CGRect oldShadowPath = CGPathGetPathBoundingBox(self.layer.shadowPath);
        CGRect newShadowPath = CGRectMake(self.bounds.origin.x - kMargin, self.bounds.origin.y, self.bounds.size.width + kMargin*2.f, self.bounds.size.height);
        self.layer.shadowPath = [UIBezierPath bezierPathWithRect:newShadowPath].CGPath;
        
        if (animated) {
            CABasicAnimation *shadowAnimation = [CABasicAnimation animationWithKeyPath:@"shadowPath"];
            shadowAnimation.fromValue = (id)[UIBezierPath bezierPathWithRect:oldShadowPath].CGPath;
            shadowAnimation.toValue = (id)[UIBezierPath bezierPathWithRect:newShadowPath].CGPath;
            shadowAnimation.duration = boundsAnimationDuration;
            [self.layer addAnimation:shadowAnimation forKey:@"shadowPath"];
        }
    }
}

- (void)updatePositionAfterRotationWithY:(CGFloat)yPos animated:(BOOL)animated {    
    CFTimeInterval positionAnimationDuration = kRotationDurationIphone; 

    BOOL isAnimating = self.isAnimating;
    CALayer *activeLayer = isAnimating ? (CALayer *)self.layer.presentationLayer : self.layer;
    NSString *currentAnimationKey = nil;
    CAMediaTimingFunction *timingFunction = nil;
    
    if (isAnimating) {
        CABasicAnimation *currentAnimation;
        if (self.state == ALAlertBannerStateShowing) {
            currentAnimation = (CABasicAnimation *)[self.layer animationForKey:kShowAlertBannerKey];
            currentAnimationKey = kShowAlertBannerKey;
        } else if (self.state == ALAlertBannerStateHiding) {
            currentAnimation = (CABasicAnimation *)[self.layer animationForKey:kHideAlertBannerKey];
            currentAnimationKey = kHideAlertBannerKey;
        } else if (self.state == ALAlertBannerStateMovingBackward || self.state == ALAlertBannerStateMovingForward) {
            currentAnimation = (CABasicAnimation *)[self.layer animationForKey:kMoveAlertBannerKey];
            currentAnimationKey = kMoveAlertBannerKey;
        } else
            return;

        CFTimeInterval remainingAnimationDuration = currentAnimation.duration - (CACurrentMediaTime() - currentAnimation.beginTime);
        timingFunction = currentAnimation.timingFunction;
        positionAnimationDuration = remainingAnimationDuration;
        
        [self.layer removeAnimationForKey:currentAnimationKey];
    }

    if (self.state == ALAlertBannerStateHiding || self.state == ALAlertBannerStateMovingBackward) {
        switch (self.position) {
            case ALAlertBannerPositionTop:
            case ALAlertBannerPositionUnderNavBar:
                yPos -= self.layer.bounds.size.height;
                break;
                
            case ALAlertBannerPositionBottom:
                yPos += self.layer.bounds.size.height;
                break;
        }
    }
    CGPoint oldPos = activeLayer.position;
    CGPoint newPos = CGPointMake(oldPos.x, yPos);
    self.layer.position = newPos;
    
    if (animated) {        
        CABasicAnimation *positionAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
        positionAnimation.fromValue = [NSValue valueWithCGPoint:oldPos];
        positionAnimation.toValue = [NSValue valueWithCGPoint:newPos];
        
        //because the banner's location is relative to the height of the screen when in the bottom position, we should just immediately set it's position upon rotation events. this will prevent any ill-timed animations due to the presentation layer's position at the time of rotation
        if (self.position == ALAlertBannerPositionBottom) {
            positionAnimationDuration = AL_DEVICE_ANIMATION_DURATION;
        }
        
        positionAnimation.duration = positionAnimationDuration;
        positionAnimation.timingFunction = timingFunction == nil ? [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear] : timingFunction;
        
        if (currentAnimationKey != nil) {
            //hijack the old animation's key value
            positionAnimation.delegate = self;
            [positionAnimation setValue:currentAnimationKey forKey:@"anim"];
        }
        
        [self.layer addAnimation:positionAnimation forKey:currentAnimationKey];
    }
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    UIColor *fillColor;
    switch (self.style) {
        case ALAlertBannerStyleSuccess:
            fillColor = [UIColor colorWithRed:(77/255.0) green:(175/255.0) blue:(67/255.0) alpha:1.f];
            break;
        case ALAlertBannerStyleFailure:
            fillColor = [UIColor colorWithRed:(173/255.0) green:(48/255.0) blue:(48/255.0) alpha:1.f];
            break;
        case ALAlertBannerStyleNotify:
            fillColor = [UIColor colorWithRed:(48/255.0) green:(110/255.0) blue:(173/255.0) alpha:1.f];
            break;
        case ALAlertBannerStyleWarning:
            fillColor = [UIColor colorWithRed:(211/255.0) green:(209/255.0) blue:(100/255.0) alpha:1.f];
            break;
    }
    
    NSArray *colorsArray = [NSArray arrayWithObjects:(id)[fillColor CGColor], (id)[[fillColor darkerColor] CGColor], nil];
    CGColorSpaceRef colorSpace =  CGColorSpaceCreateDeviceRGB();
    const CGFloat locations[2] = {0.f, 1.f};
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)colorsArray, locations);
    
    CGContextDrawLinearGradient(context, gradient, CGPointZero, CGPointMake(0.f, self.bounds.size.height), 0.f);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
    
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:0.f green:0.f blue:0.f alpha:0.6f].CGColor);
    CGContextFillRect(context, CGRectMake(0.f, rect.size.height - 1.f, rect.size.width, 1.f));
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:1.f green:1.f blue:1.f alpha:0.3f].CGColor);
    CGContextFillRect(context, CGRectMake(0.f, 0.f, rect.size.width, 1.f));
}

- (id)nextAvailableViewController:(id)view {
    id nextResponder = [view nextResponder];
    if ([nextResponder isKindOfClass:[UIViewController class]]) {
        return nextResponder;
    } else if ([nextResponder isKindOfClass:[UIView class]]) {
        return [self nextAvailableViewController:nextResponder];
    } else {
        return nil;
    }
}

- (NSString *)description {
    NSString *styleString;
    switch (self.style) {
        case ALAlertBannerStyleSuccess:
            styleString = @"ALAlertBannerStyleSuccess";
            break;
        case ALAlertBannerStyleFailure:
            styleString = @"ALAlertBannerStyleFailure";
            break;
        case ALAlertBannerStyleNotify:
            styleString = @"ALAlertBannerStyleNotify";
            break;
        case ALAlertBannerStyleWarning:
            styleString = @"ALAlertBannerStyleWarning";
            break;
    }
    NSString *positionString;
    switch (self.position) {
        case ALAlertBannerPositionTop:
            positionString = @"ALAlertBannerPositionTop";
            break;
        case ALAlertBannerPositionBottom:
            positionString = @"ALAlertBannerPositionBottom";
            break;
        case ALAlertBannerPositionUnderNavBar:
            positionString = @"ALAlertBannerPositionUnderNavBar";
            break;
    }
    NSString *descriptionString = [NSString stringWithFormat:@"<%@: %p; frame = %@; style = %@; position = %@; superview = <%@: %p>", self.class, self, NSStringFromCGRect(self.frame), styleString, positionString, self.superview.class, self.superview];
    return descriptionString;
}

@end
