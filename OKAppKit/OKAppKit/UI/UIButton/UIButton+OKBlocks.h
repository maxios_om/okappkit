//
//  UIButton+OKBlocks.h
//  OKAppKit
//
//  Created by Omkar khedekar on 05/03/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ButtonBlock)(UIButton* sender);
@interface UIButton (OKBlocks)

- (void)addAction:(ButtonBlock)block
  forControlEvent:(UIControlEvents)controlEvent;

/* does not support remove one specific block*/

- (void)removeAllActionForControlEvent:(UIControlEvents)controlEvent;

@end
