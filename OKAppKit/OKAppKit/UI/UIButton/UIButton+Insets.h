//
//  UIButton+ImageTitleCentering.h
//  OKAppKit
//
//  Created by Omkar khedekar on 24/03/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Insets)

- (void)centerButtonAndImageWithSpacing:(CGFloat)spacing;

- (void)addSpaceBetweenImageAndText:(CGFloat)space;

- (void)addSpaceBetweenImageAndText:(CGFloat)space withLeftAndRightPadding:(CGFloat)padding;

- (void)positionTextBelowImage;

@end
