//
//  UIButton+ImageTitleCentering.m
//  OKAppKit
//
//  Created by Omkar khedekar on 24/03/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import "UIButton+Insets.h"

@implementation UIButton (Insets)

- (void)centerButtonAndImageWithSpacing:(CGFloat)spacing {
    CGFloat insetAmount = spacing / 2.0;
    self.imageEdgeInsets = UIEdgeInsetsMake(0, -insetAmount, 0, insetAmount);
    self.titleEdgeInsets = UIEdgeInsetsMake(0, insetAmount, 0, -insetAmount);
    self.contentEdgeInsets = UIEdgeInsetsMake(0, insetAmount, 0, insetAmount);
}

-(void)addSpaceBetweenImageAndText:(CGFloat)space {
    self.titleEdgeInsets = UIEdgeInsetsMake(0.0, space, 0.0, -space);
    self.contentEdgeInsets = UIEdgeInsetsMake(0.0, space, 0.0, 2.0 * space);
}

- (void)addSpaceBetweenImageAndText:(CGFloat)space withLeftAndRightPadding:(CGFloat)padding {
    self.titleEdgeInsets = UIEdgeInsetsMake(0.0, space, 0.0, -space);
    self.contentEdgeInsets = UIEdgeInsetsMake(0.0, padding, 0.0, padding);
}


- (void)positionTextBelowImage {
    self.titleEdgeInsets = UIEdgeInsetsMake(0.0, -[self imageForState:UIControlStateNormal].size.width, -self.titleLabel.frame.size.height - 10.0, 0.0);
    self.imageEdgeInsets = UIEdgeInsetsMake(-self.titleLabel.frame.size.height, 0.0, 0.0, -self.titleLabel.frame.size.width);
    [self sizeToFit];
}
@end
