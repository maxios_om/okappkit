//
//  UIViewController+Utils.m
//  HadHud
//
//  Created by Omkar on 5/19/15.
//  Copyright (c) 2015 HadHud. All rights reserved.
//

#import "UIViewController+Utils.h"

@implementation UIViewController (Utils)


+ (UIViewController*) ok_getTopViewController
{
    UIViewController *topViewController = [UIApplication sharedApplication].keyWindow.rootViewController;

    while (topViewController.presentedViewController) {
        topViewController = topViewController.presentedViewController;
    }

    return topViewController;
}

+ (instancetype) instanceFromStoryBoard:(UIStoryboard *) storyBoard {
    
    return [storyBoard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}


- (BOOL)isVisible {
    return [self isViewLoaded] && self.view.window;
}
@end
