//
//  UIViewController+Reachability.h
//  HadHud
//
//  Created by Omkar on 3/26/15.
//  Copyright (c) 2015 HadHud. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface UIViewController (Reachability)

- (void)ok_registerForNetworkReachabilityNotifications;
- (void)ok_unsubscribeFromNetworkReachabilityNotifications;
- (void)ok_checkForNetworkConnectivity;


@end
