//
//  UIViewController+Reachability.m
//  HadHud
//
//  Created by Omkar on 3/26/15.
//  Copyright (c) 2015 HadHud. All rights reserved.
//

#import "UIViewController+Reachability.h"
#import "OKReachability.h"
#import "AbstractViewController.h"
#import "OKAppKitLocalization.h"
#import "ALAlertBanner.h"
#import "ALAlertBannerManager.h"

@implementation UIViewController (Reachability)


#pragma mark reachability



- (void)ok_registerForNetworkReachabilityNotifications
{
    
    [[OKReachabilityAdaptor sharedAdapter] startNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];

}


- (void)ok_unsubscribeFromNetworkReachabilityNotifications
{
    [[OKReachabilityAdaptor sharedAdapter] stopNotifications];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

- (void)showNotificationForStatus:(NetworkStatus)currentReachabilityStatus
{
    if (NotReachable == currentReachabilityStatus ) {
        
        ALAlertBanner *banner = [ALAlertBanner alertBannerForView:self.view
                                                            style:ALAlertBannerStyleWarning
                                                         position:ALAlertBannerPositionTop
                                                            title:OKLocalisedString(@"Connectivity")
                                                         subtitle:OKLocalisedString(@"The internet connection seems to be down. Please check that!")];
        [banner show];

    } else {

        [[ALAlertBannerManager sharedManager] hideAlertBannersInView:self.view];
    }
}

- (void)reachabilityChanged:(NSNotification *)note
{

    NetworkStatus currentReachabilityStatus = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];

    BOOL showNotification = YES;

    if ([self conformsToProtocol:@protocol(AbstractViewControllerProtocol)]) {

        if ([self respondsToSelector:@selector(reachabilityDidChanged:)]) {

            [(AbstractViewController *) self reachabilityDidChanged:(NetworkReachabilityStatus)currentReachabilityStatus];

        }

        if ([self respondsToSelector:@selector(shouldDisplayReachabilityStatusNotifications)]) {

            id <AbstractViewControllerProtocol> protocaldSelf = (id <AbstractViewControllerProtocol> ) self;

             showNotification = [protocaldSelf shouldDisplayReachabilityStatusNotifications];
        }
    }

    if (showNotification) {
        [self showNotificationForStatus:currentReachabilityStatus];
    }

    
}


-(void) ok_checkForNetworkConnectivity {

    [self reachabilityChanged:nil];
}
@end
