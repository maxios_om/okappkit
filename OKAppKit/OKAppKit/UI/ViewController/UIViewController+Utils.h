//
//  UIViewController+Utils.h
//  HadHud
//
//  Created by Omkar on 5/19/15.
//  Copyright (c) 2015 HadHud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Utils)

+ (UIViewController*) ok_getTopViewController;

+ (instancetype) instanceFromStoryBoard:(UIStoryboard *) storyBoard;

- (BOOL)isVisible;

@end
