//
//  OKDatePickerViewController.h
//  OKAppKit
//
//  Created by Omkar khedekar on 20/03/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 *  The Block which is executed when the select button is touched.
 *
 *  @param date The date that is picked.
 *
 */
typedef void (^OKDatePickerDateBlock)(NSDate *date);

/**
 *  The Block which is executed when the cancel button is touched.
 */
typedef void (^OKDatePickerCancelBlock)(void);

@interface OKDatePickerViewController : UIViewController

/**
 *  The `UIDatePicker` object used in the component.
 */
@property (nonatomic) UIDatePicker *datePicker;

/**
 * The Block which is executed when the select button is touched.
 */
@property (nonatomic, copy) OKDatePickerDateBlock dateBlock;

/**
 *  The Block which is executed when the cancel button is touched.
 */
@property (nonatomic, copy) OKDatePickerCancelBlock cancelBlock;

/**
 *  Creates and returns a new date picker.
 *
 *  @param date          The initial date. A `nil` value will set the current date.
 *  @param selectedBlock The Block which is executed when the select button is touched.
 *  @param cancelBlock   The Block which is executed when the cancel button is touched.
 *
 *  @return A newly created date picker.
 */
+ (id)pickerWithDate:(NSDate *)date selectedBlock:(OKDatePickerDateBlock)selectedBlock cancelBlock:(OKDatePickerCancelBlock)cancelBlock;

@end
