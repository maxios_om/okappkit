//
//  UIImage+OK_Utils.h
//  OK_Utils
//
//  Created by Omkar on 09/07/13.
//  Copyright (c) 2013 Oms. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (OK_Utils)
/**
 *  <#Description#>
 *
 *  @param color <#color description#>
 *
 *  @return <#return value description#>
 */
+ (UIImage *) ok_imageWithColor:(UIColor *)color;

/**
 *  <#Description#>
 *
 *  @param color <#color description#>
 *  @param size  <#size description#>
 *
 *  @return <#return value description#>
 */
+ (UIImage *) ok_imageWithColor:(UIColor *)color andSize:(CGSize)size;

/**
 *
 *
 *  @return <#return value description#>
 */
+ (UIImage *) screenshot;

/**
 *
 *
 *  @param width <#width description#>
 *
 *  @return <#return value description#>
 */
- (UIImage *) resizedImageByWidth:  (NSUInteger) width;

/**
 *
 *
 *  @param height <#height description#>
 *
 *  @return <#return value description#>
 */
- (UIImage *) resizedImageByHeight:  (NSUInteger) height;

/**
 *  <#Description#>
 *
 *  @param size <#size description#>
 *
 *  @return <#return value description#>
 */
- (UIImage *) resizedImageWithMinimumSize: (CGSize) size;

/**
 *  <#Description#>
 *
 *  @param size <#size description#>
 *
 *  @return <#return value description#>
 */
- (UIImage *) resizedImageWithMaximumSize: (CGSize) size;

/**
 *  <#Description#>
 *
 *  @param bounds <#bounds description#>
 *
 *  @return <#return value description#>
 */
- (UIImage *) drawImageInBounds: (CGRect) bounds;

/**
 *  <#Description#>
 *
 *  @param rect <#rect description#>
 *
 *  @return <#return value description#>
 */
- (UIImage *) croppedImageWithRect: (CGRect) rect;

/**
 *  <#Description#>
 *
 *  @return <#return value description#>
 */
- (BOOL)hasAlpha;

/**
 *  <#Description#>
 *
 *  @return <#return value description#>
 */
- (UIImage *)imageWithAlpha;

/**
 *  <#Description#>
 *
 *  @param borderSize <#borderSize description#>
 *
 *  @return <#return value description#>
 */
- (UIImage *)transparentBorderImage:(NSUInteger)borderSize;

/**
 *  <#Description#>
 *
 *  @param cornerSize <#cornerSize description#>
 *  @param borderSize <#borderSize description#>
 *
 *  @return <#return value description#>
 */
- (UIImage *)roundedCornerImage:(NSInteger)cornerSize borderSize:(NSInteger)borderSize;


+(instancetype) imageWithBase64EncodedString:(NSString *) base64IncodedImage options:(NSDataBase64DecodingOptions)options;

+(instancetype) imageWithBase64EncodedString:(NSString *)base64IncodedImage;
@end
