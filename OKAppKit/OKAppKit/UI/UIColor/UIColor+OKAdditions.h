//
//  UIColor+OKokitions.h
//  OKAppKit
//
//  Created by Omkar khedekar on 15/02/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * The color type (unsigned int).
 **/
typedef unsigned int OKColorType;

@interface UIColor (OKAdditions)
/** *************************************************** **
 * @name Hexadecimal support
 ** *************************************************** **/

/**
 * Creates a color from a RGB hexadecimal integer value.
 * @param rgbHexValue The RGB color represented in an hexadecimal integer.
 * @return The `UIColor` instance.
 **/
+ (UIColor*)ok_colorWithRGBHexValue:(OKColorType)rgbHexValue;

/**
 * Creates a color from a RGBA hexadecimal integer value.
 * @param rgbaHexValue The RGBA color represented in an hexadecimal integer.
 * @return The `UIColor` instance.
 **/
+ (UIColor*)ok_colorWithRGBAHexValue:(OKColorType)rgbaHexValue;

/**
 * Creates a color from a RGB hexadecimal string value.
 * @param rgbHexString The RGB color represented in an hexadecimal string.
 * @return The `UIColor` instance.
 **/
+ (UIColor*)ok_colorWithRGBHexString:(NSString*)rgbHexString;

/**
 * Creates a color from a RGBA hexadecimal string value.
 * @param rgbaHexString The RGBA color represented in an hexadecimal string.
 * @return The `UIColor` instance.
 **/
+ (UIColor*)ok_colorWithRGBAHexString:(NSString*)rgbaHexString;

/**
 * Retrieves the RGB hexadecimal integer value.
 * @param rgbHexValue A pointer to an integer where the value will be written.
 * @return YES if the color description could have been retrevied, otherwise NO.
 **/
- (BOOL)ok_getRGBHexValue:(OKColorType*)rgbHexValue;

/**
 * Retrieves the RGBA hexadecimal integer value.
 * @param rgbaHexValue A pointer to an integer where the value will be written.
 * @return YES if the color description could have been retrevied, otherwise NO.
 **/
- (BOOL)ok_getRGBAHexValue:(OKColorType*)rgbaHexValue;

/**
 * Retrieves the RGB hexadecimal string value.
 * @return The RGB hexadecimal string representation or nil.
 **/
- (NSString*)ok_RGBHexString;

/**
 * Retrieves the RGBA hexadecimal string value.
 * @return The RGBA hexadecimal string representation or nil.
 **/
- (NSString*)ok_RGBAHexString;

/** *************************************************** **
 * @name RGB support
 ** *************************************************** **/

/**
 * Creates a color from RGB (0-255 range) values.
 * @param red The red value in 0-255 range.
 * @param green The green value in 0-255 range.
 * @param blue The blue value in 0-255 range.
 * @return The new color from the RGB values.
 **/
+ (UIColor*)ok_colorWithRed255:(CGFloat)red green255:(CGFloat)green blue255:(CGFloat)blue;

/**
 * Creates a color from RGBA (0-255 range) values.
 * @param red The red value in 0-255 range.
 * @param green The green value in 0-255 range.
 * @param blue The blue value in 0-255 range.
 * @param alpha The alpha value in 0-255 range.
 * @return The new color from the RGB values.
 **/
+ (UIColor*)ok_colorWithRed255:(CGFloat)red green255:(CGFloat)green blue255:(CGFloat)blue alpha255:(CGFloat)alpha;

/** *************************************************** **
 * @name Luminiscence
 ** *************************************************** **/

/**
 * Converts the current color into grayscale.
 * @return The new grayscale color.
 * @discussion This method uses the formula of luminiscence of L=R*0.2989+G*0.5870+B*0.1140.
 **/
- (UIColor*)ok_grayColor;

/**
 * A convenience method that indicates if a color is considered "light".
 * @return YES if the color is considered "light", NO otherwise.
 **/
- (BOOL)ok_isLightColor;

/**
 * A convenience method that indicates if a color is considered "dark".
 * @return YES if the color is considered "dark", NO otherwise.
 **/
- (BOOL)ok_isDarkColor;

/** *************************************************** **
 * @name Modifying colors
 ** *************************************************** **/

/**
 * Creates a new color from the current one by changing the saturation.
 * newSaturation
 * @return A new color.
 **/
- (UIColor*)ok_colorWithSaturation:(CGFloat)newSaturation;

/**
 * Creates a new color from the current one by changing the brightness.
 * newBrightness
 * @return A new color.
 **/
- (UIColor*)ok_colorWithBrightness:(CGFloat)newBrightness;

/**
 * Creates a new lighter color.
 * @param value Float value from 0 to 1 where 0 means no change and 1 means the lightest possible color.
 * @return A new color.
 **/
- (UIColor*)ok_lighterColorWithValue:(CGFloat)value;

/**
 * Creates a new darker color.
 * @param value Float value from 0 to 1 where 0 means no change and 1 means the darkest possible color.
 * @return A new color.
 **/
- (UIColor*)ok_darkerColorWithValue:(CGFloat)value;


+ (UIColor *) ok_colorWithHexString: (NSString *) hexString;
+ (UIColor *) ok_colorWithHexString: (NSString *) hexString alpha:(CGFloat) alpha;
@end
