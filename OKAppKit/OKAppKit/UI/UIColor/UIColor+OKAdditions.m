//
//  UIColor+OKOKitions.m
//  OKAppKit
//
//  Created by Omkar khedekar on 15/02/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import "UIColor+OKAdditions.h"
#define OK_RED_MASK        0xFF0000
#define OK_GREEN_MASK      0xFF00
#define OK_BLUE_MASK       0xFF
#define OK_ALPHA_MASK      0xFF000000

#define OK_RED_SHIFT       16
#define OK_GREEN_SHIFT     8
#define OK_BLUE_SHIFT      0
#define OK_ALPHA_SHIFT     24

#define OK_COLOR_SIZE      255.0

#define OK_LUM_RED         0.2989
#define OK_LUM_GREEN       0.5870
#define OK_LUM_BLUE        0.1140

@implementation UIColor (OKAdditions)
+ (UIColor*)ok_colorWithRGBHexValue:(OKColorType)rgbValue
{
    return [UIColor colorWithRed:((CGFloat)((rgbValue & OK_RED_MASK) >> OK_RED_SHIFT))/OK_COLOR_SIZE
                           green:((CGFloat)((rgbValue & OK_GREEN_MASK) >> OK_GREEN_SHIFT))/OK_COLOR_SIZE
                            blue:((CGFloat)((rgbValue & OK_BLUE_MASK) >> OK_BLUE_SHIFT))/OK_COLOR_SIZE
                           alpha:1.0];
}

+ (UIColor*)ok_colorWithRGBAHexValue:(OKColorType)rgbaValue
{
    return [UIColor colorWithRed:((CGFloat)((rgbaValue & OK_RED_MASK) >> OK_RED_SHIFT))/OK_COLOR_SIZE
                           green:((CGFloat)((rgbaValue & OK_GREEN_MASK) >> OK_GREEN_SHIFT))/OK_COLOR_SIZE
                            blue:((CGFloat)((rgbaValue & OK_BLUE_MASK) >> OK_BLUE_SHIFT))/OK_COLOR_SIZE
                           alpha:((CGFloat)((rgbaValue & OK_ALPHA_MASK) >> OK_ALPHA_SHIFT))/OK_COLOR_SIZE];
}

+ (UIColor*)ok_colorWithRGBHexString:(NSString*)rgbStrValue
{
    OKColorType rgbHexValue;
    
    NSScanner* scanner = [NSScanner scannerWithString:rgbStrValue];
    BOOL successful = [scanner scanHexInt:&rgbHexValue];
    
    if (!successful)
        return nil;
    
    return [self ok_colorWithRGBHexValue:rgbHexValue];
}

+ (UIColor*)ok_colorWithRGBAHexString:(NSString*)rgbaStrValue
{
    OKColorType rgbHexValue;
    
    NSScanner *scanner = [NSScanner scannerWithString:rgbaStrValue];
    BOOL successful = [scanner scanHexInt:&rgbHexValue];
    
    if (!successful)
        return nil;
    
    return [self ok_colorWithRGBAHexValue:rgbHexValue];
}

- (BOOL)ok_getRGBHexValue:(OKColorType*)rgbHex
{
    size_t numComponents = CGColorGetNumberOfComponents(self.CGColor);
    CGFloat const * components = CGColorGetComponents(self.CGColor);
    
    if (numComponents == 4)
    {
        CGFloat rFloat = components[0]; // red
        CGFloat gFloat = components[1]; // green
        CGFloat bFloat = components[2]; // blue
        
        OKColorType r = (OKColorType)roundf(rFloat*OK_COLOR_SIZE);
        OKColorType g = (OKColorType)roundf(gFloat*OK_COLOR_SIZE);
        OKColorType b = (OKColorType)roundf(bFloat*OK_COLOR_SIZE);
        
        *rgbHex = (r << OK_RED_SHIFT) + (g << OK_GREEN_SHIFT) + (b << OK_BLUE_SHIFT);
        
        return YES;
    }
    else if (numComponents == 2)
    {
        CGFloat gFloat = components[0]; // gray
        
        OKColorType g = (OKColorType)roundf(gFloat*OK_COLOR_SIZE);
        
        *rgbHex = (g << OK_RED_SHIFT) + (g << OK_GREEN_SHIFT) + (g << OK_BLUE_SHIFT);
        
        return YES;
    }
    
    return NO;
}

- (BOOL)ok_getRGBAHexValue:(OKColorType*)rgbaHex;
{
    size_t numComponents = CGColorGetNumberOfComponents(self.CGColor);
    CGFloat const * components = CGColorGetComponents(self.CGColor);
    
    if (numComponents == 4)
    {
        CGFloat rFloat = components[0]; // red
        CGFloat gFloat = components[1]; // green
        CGFloat bFloat = components[2]; // blue
        CGFloat aFloat = components[3]; // alpha
        
        OKColorType r = (OKColorType)roundf(rFloat*OK_COLOR_SIZE);
        OKColorType g = (OKColorType)roundf(gFloat*OK_COLOR_SIZE);
        OKColorType b = (OKColorType)roundf(bFloat*OK_COLOR_SIZE);
        OKColorType a = (OKColorType)roundf(aFloat*OK_COLOR_SIZE);
        
        *rgbaHex = (r << OK_RED_SHIFT) + (g << OK_GREEN_SHIFT) + (b << OK_BLUE_SHIFT) + (a << OK_ALPHA_SHIFT);
        
        return YES;
    }
    else if (numComponents == 2)
    {
        CGFloat gFloat = components[0]; // gray
        CGFloat aFloat = components[1]; // alpha
        
        OKColorType g = (OKColorType)roundf(gFloat*OK_COLOR_SIZE);
        OKColorType a = (OKColorType)roundf(aFloat *OK_COLOR_SIZE);
        
        *rgbaHex = (g << OK_RED_SHIFT) + (g << OK_GREEN_SHIFT) + (g << OK_BLUE_SHIFT) + (a << OK_ALPHA_SHIFT);
        
        return YES;
    }
    
    return NO;
}

- (OKColorType)ok_RGBHexValue
{
    return 0;
}

- (OKColorType)ok_RGBAHexValue
{
    return 0;
}

- (NSString*)ok_RGBHexString
{
    OKColorType value = 0;
    BOOL compatible = [self ok_getRGBHexValue:&value];
    
    if (!compatible)
        return nil;
    
    return [NSString stringWithFormat:@"%x", value];
}

- (NSString*)ok_RGBAHexString
{
    OKColorType value = 0;
    BOOL compatible = [self ok_getRGBAHexValue:&value];
    
    if (!compatible)
        return nil;
    
    return [NSString stringWithFormat:@"%x", value];
}


+ (UIColor*)ok_colorWithRed255:(CGFloat)red green255:(CGFloat)green blue255:(CGFloat)blue
{
    return [self ok_colorWithRed255:red green255:green blue255:blue alpha255:OK_COLOR_SIZE];
}

+ (UIColor*)ok_colorWithRed255:(CGFloat)red green255:(CGFloat)green blue255:(CGFloat)blue alpha255:(CGFloat)alpha
{
    return [UIColor colorWithRed:red/OK_COLOR_SIZE green:green/OK_COLOR_SIZE blue:blue/OK_COLOR_SIZE alpha:alpha/OK_COLOR_SIZE];
}

- (UIColor*)ok_grayColor
{
    size_t totalComponents = CGColorGetNumberOfComponents(self.CGColor);
    BOOL isGreyscale = (totalComponents == 2) ? YES : NO;
    
    CGFloat const * components = (CGFloat *)CGColorGetComponents(self.CGColor);
    CGFloat luminiscence = 0.0;
    CGFloat alpha = 1.0;
    
    if (isGreyscale)
    {
        luminiscence = components[0];
        alpha = components[1];
    }
    else
    {
        luminiscence = components[0]*OK_LUM_RED + components[1]*OK_LUM_GREEN + components[2]*OK_LUM_BLUE;
        alpha = components[3];
    }
    
    return [UIColor colorWithWhite:luminiscence alpha:alpha];
}

- (UIColor*)ok_colorWithSaturation:(CGFloat)newSaturation
{
    CGFloat hue,saturation,brightness,alpha;
    [self getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
    
    return [UIColor colorWithHue:hue saturation:newSaturation brightness:brightness alpha:alpha];
}

- (UIColor*)ok_colorWithBrightness:(CGFloat)newBrightness
{
    CGFloat hue,saturation,brightness,alpha;
    [self getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
    
    return [UIColor colorWithHue:hue saturation:saturation brightness:newBrightness alpha:alpha];
}


- (UIColor*)ok_lighterColorWithValue:(CGFloat)value
{
    size_t totalComponents = CGColorGetNumberOfComponents(self.CGColor);
    BOOL isGreyscale = (totalComponents == 2) ? YES : NO;
    
    CGFloat const * oldComponents = (CGFloat *)CGColorGetComponents(self.CGColor);
    CGFloat newComponents[4];
    
    CGFloat (^actionBlock)(CGFloat component) = ^CGFloat(CGFloat component) {
        
        CGFloat offset = 1.0 - component;
        CGFloat newComponent = component + offset*value;
        
        // CGFloat newComponent = component + value > 1.0 ? 1.0 : component + value;
        
        return newComponent;
    };
    
    if (isGreyscale)
    {
        newComponents[0] = actionBlock(oldComponents[0]);
        newComponents[1] = actionBlock(oldComponents[0]);
        newComponents[2] = actionBlock(oldComponents[0]);
        newComponents[3] = oldComponents[1];
    }
    else
    {
        newComponents[0] = actionBlock(oldComponents[0]);
        newComponents[1] = actionBlock(oldComponents[1]);
        newComponents[2] = actionBlock(oldComponents[2]);
        newComponents[3] = oldComponents[3];
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGColorRef newColor = CGColorCreate(colorSpace, newComponents);
    CGColorSpaceRelease(colorSpace);
    
    UIColor *retColor = [UIColor colorWithCGColor:newColor];
    CGColorRelease(newColor);
    
    return retColor;
}

- (UIColor*)ok_darkerColorWithValue:(CGFloat)value
{
    size_t totalComponents = CGColorGetNumberOfComponents(self.CGColor);
    BOOL isGreyscale = (totalComponents == 2) ? YES : NO;
    
    CGFloat const * oldComponents = (CGFloat *)CGColorGetComponents(self.CGColor);
    CGFloat newComponents[4];
    
    CGFloat (^actionBlock)(CGFloat component) = ^CGFloat(CGFloat component) {
        
        CGFloat newComponent = component * (1.0 - value);
        
        // CGFloat newComponent = component - value < 0.0 ? 0.0 : component - value;
        
        return newComponent;
    };
    
    if (isGreyscale)
    {
        newComponents[0] = actionBlock(oldComponents[0]);
        newComponents[1] = actionBlock(oldComponents[0]);
        newComponents[2] = actionBlock(oldComponents[0]);
        newComponents[3] = oldComponents[1];
    }
    else
    {
        newComponents[0] = actionBlock(oldComponents[0]);
        newComponents[1] = actionBlock(oldComponents[1]);
        newComponents[2] = actionBlock(oldComponents[2]);
        newComponents[3] = oldComponents[3];
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGColorRef newColor = CGColorCreate(colorSpace, newComponents);
    CGColorSpaceRelease(colorSpace);
    
    UIColor *retColor = [UIColor colorWithCGColor:newColor];
    CGColorRelease(newColor);
    
    return retColor;
}

- (BOOL)ok_isLightColor
{
    size_t totalComponents = CGColorGetNumberOfComponents(self.CGColor);
    BOOL isGreyscale = (totalComponents == 2) ? YES : NO;
    
    CGFloat const * components = (CGFloat *)CGColorGetComponents(self.CGColor);
    CGFloat sum;
    
    if (isGreyscale)
        sum = components[0];
    else
        sum = components[0]*OK_LUM_RED + components[1]*OK_LUM_GREEN + components[2]*OK_LUM_BLUE;
    
    return (sum > 0.8f);
}

- (BOOL)ok_isDarkColor
{
    return ![self ok_isLightColor];
}


+ (UIColor *) ok_colorWithHexString: (NSString *) hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self ok_colorComponentFrom: colorString start: 0 length: 1];
            green = [self ok_colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self ok_colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self ok_colorComponentFrom: colorString start: 0 length: 1];
            red   = [self ok_colorComponentFrom: colorString start: 1 length: 1];
            green = [self ok_colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self ok_colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self ok_colorComponentFrom: colorString start: 0 length: 2];
            green = [self ok_colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self ok_colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self ok_colorComponentFrom: colorString start: 0 length: 2];
            red   = [self ok_colorComponentFrom: colorString start: 2 length: 2];
            green = [self ok_colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self ok_colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            return nil;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

//

+ (UIColor *) ok_colorWithHexString: (NSString *) hexString alpha:(CGFloat) alpha {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat tAlpha, red, blue, green;
    
    
    switch ([colorString length]) {
        case 3: // #RGB
            tAlpha = alpha;
            red   = [self ok_colorComponentFrom: colorString start: 0 length: 1];
            green = [self ok_colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self ok_colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            tAlpha = [self ok_colorComponentFrom: colorString start: 0 length: 1];
            red   = [self ok_colorComponentFrom: colorString start: 1 length: 1];
            green = [self ok_colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self ok_colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            tAlpha = 1.0f;
            red   = [self ok_colorComponentFrom: colorString start: 0 length: 2];
            green = [self ok_colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self ok_colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            tAlpha = [self ok_colorComponentFrom: colorString start: 0 length: 2];
            red   = [self ok_colorComponentFrom: colorString start: 2 length: 2];
            green = [self ok_colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self ok_colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            return nil;
    }
    
    if (alpha > 0 && tAlpha != alpha) {
        tAlpha = alpha;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: tAlpha];
}

+(UIColor *) ok_randomColor {
    
    CGFloat red = arc4random_uniform(255)  / 255.0f;
    CGFloat blue = arc4random_uniform(255)  / 255.0f;
    CGFloat green = arc4random_uniform(255) / 255.0f;
    UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:1];
    return color;
    
}

+ (CGFloat) ok_colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

@end
