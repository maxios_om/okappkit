//
//  OKAlertView.m
//  OKAppKit
//
//  Created by Omkar khedekar on 20/02/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import "OKAlertView.h"

@interface OKAlertAction()

@property (nonatomic, readwrite) NSString *title;
@property (nonatomic, readwrite) OKAlertActionStyle style;
@property (nonatomic, copy) OKAlertViewActionsHandler actionHandler;

- (OKAlertAction *)initWithTitle:title style:(OKAlertActionStyle)style handler:(OKAlertViewActionsHandler)handler;

@end


@implementation OKAlertAction

- (OKAlertAction *)initWithTitle:title style:(OKAlertActionStyle)style handler:(OKAlertViewActionsHandler)handler {
    if (self = [super init]) {
        self.title = title;
        self.style = style;
        self.actionHandler = handler;
    }
    return self;
}

+ (OKAlertAction *)actionWithTitle:title style:(OKAlertActionStyle)style handler:(OKAlertViewActionsHandler)handler {
    OKAlertAction *action = [[OKAlertAction alloc] initWithTitle:title style:style handler:handler];
    return action;
}

@end


@interface OKAlertView()

@property (nonatomic, strong) NSArray *actions; // KCAlertAction

@end

@interface OKAlertView ()
@property (nullable, nonatomic, strong) id activeAlert;
@end

@implementation OKAlertView

+ (instancetype)sharedInstance {
    
    static dispatch_once_t onceToken;
    static OKAlertView *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[OKAlertView alloc] init];
    });
    return instance;
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    OKAlertAction *action = self.actions[buttonIndex];
    if (action && action.handler) {
        action.handler(action);
    }
}

- (void)showAlertWithViewController:(UIViewController *)viewController title:(NSString *)title message:(NSString *)message actions:(NSArray <OKAlertAction *> *)actions {
#if _DEBUG
    for (KCAlertAction *action in actions) {
        NSAssert([action isKindOfClass:[KCAlertAction class]], @"Invalid action, actions must contain KCAlertAction objects.");
    }
#endif
    [self dismissAllActiveAlerts];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        for (OKAlertAction *action in actions) {
            [alertController addAction:[UIAlertAction actionWithTitle:action.title style:(UIAlertActionStyle)action.style handler:^(UIAlertAction *alertAction) {
                if (action && action.handler) {
                    action.handler(action);
                }
            }]];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[OKAlertView sharedInstance] setActiveAlert:alertController];
            [viewController presentViewController:alertController animated:TRUE completion:nil];
        });
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] init];
        alertView.delegate = self;
        alertView.title = title;
        alertView.message = message;
        
        // ALways put the cancel last
        self.actions = [actions sortedArrayWithOptions:0 usingComparator:^NSComparisonResult(OKAlertAction *obj1, OKAlertAction *obj2) {
            return (obj1.style == UIAlertActionStyleCancel) ? NSOrderedDescending : NSOrderedSame;
        }];
        
        for (OKAlertAction *action in self.actions) {
            NSInteger index = [alertView addButtonWithTitle:action.title];
            
            if (action.style == UIAlertActionStyleCancel) {
                alertView.cancelButtonIndex = index;
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[OKAlertView sharedInstance] setActiveAlert:alertView];
            [alertView show];
        });
    }
}


-(void) dismissAllActiveAlerts {
    if (!self.activeAlert) return;
    
    
    if ([self.activeAlert isKindOfClass:[UIAlertController class]]) {
        UIAlertController *controller = (UIAlertController *) self.activeAlert;
        
        [controller dismissViewControllerAnimated:YES completion:nil];
    }
    
    if ([self.activeAlert isKindOfClass:[UIAlertView class]]) {
        UIAlertView *controller = (UIAlertView *) self.activeAlert;
        
        [controller dismissWithClickedButtonIndex:controller.cancelButtonIndex animated:YES];
    }
}
@end
