//
//  OKAlertView.h
//  OKAppKit
//
//  Created by Omkar khedekar on 20/02/16.
//  Copyright © 2016 HadHud. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, OKAlertActionStyle) {
    OKAlertActionStyleDefault = 0,
    OKAlertActionStyleCancel,
    OKAlertActionStyleDestructive
};

typedef NS_ENUM(NSInteger, OKAlertControllerStyle) {
    OKAlertControllerStyleActionSheet = 0,
    OKAlertControllerStyleAlert
};

@class OKAlertAction;

typedef void (^OKAlertViewActionsHandler)( OKAlertAction * _Nullable action);

@interface OKAlertAction : NSObject

+ (nonnull OKAlertAction *)actionWithTitle:(nullable NSString *)title
                                     style:(OKAlertActionStyle)style
                                   handler:(nullable OKAlertViewActionsHandler)handler;

@property (nullable, nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) OKAlertActionStyle style;
@property (nullable, nonatomic, readonly) OKAlertViewActionsHandler handler;

@end



@interface OKAlertView : NSObject <UIAlertViewDelegate>

+ (nonnull instancetype)sharedInstance;

- (void)showAlertWithViewController:(nullable UIViewController *)viewController
                                     title:(nullable NSString *)title
                                   message:(nullable NSString *)message
                                   actions:(nullable NSArray <OKAlertAction *> *)actions;

-(void) dismissAllActiveAlerts;

@end
